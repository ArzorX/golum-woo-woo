;;============================
;;★BGM　フィールド
@bgm f=Theme1

@bg f=森
@fade type=in t=500 wt=1

;;★SE　歩く
@se f=歩く（革靴・アスファルト）
@wait t=100

[Rick]「Hoah, hoo......I'm tired after such a long way.」

 My feet hurt after walking so long.

[Rick]「Find a place to have a rest......」

 I look around to examine whether the sword would appear.

[Rick]「Um? There is a house there.」

 There is an old house not far away from me.

[Rick]「It seems to be no monster girl around......」

 Have a rest in the old house.

[Rick](Anybody......lives there?)

 I expect to see human beings and go to that house.

@ev f=森

@move f=森 x=0 y=19  w=105 h=105 opa=255 t=200 wt=1
@move f=森 x=0 y=-19 w=110 h=110 opa=255 t=200 wt=1

@move f=森 x=0 y=19  w=115 h=115 opa=255 t=200 wt=1
@move f=森 x=0 y=-19 w=120 h=120 opa=255 t=200 wt=1

@bs
@se
@bgm
@bgs
@bgv
@rps

;;============================
