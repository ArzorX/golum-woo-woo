;;============================
;;------------------------------------------------------
;;戦闘2～5回目　敗北
;;------------------------------------------------------

;;★BGM　敗北
@se f=風切り音2
@flash t=1000 wt=0
@fade type=out t=20 wt=1
@move_f f=リザードマン@ out=c t=20 wt=0
@bs wt=0

@ev f=街道 wt=0
@bs f=リザードマン@焦り_頬 face=0 body=-1 t=20 wt=0
@bs f=effect/集中線@01 face=0 body=-1 t=20 opa=70 w=0
@move f=街道 x=0 y=0 w=130 h=130 opa=255 t=20 wt=1

@fade type=in t=20 wt=1

@bgm
@wait t=250

@se f=Blow4
@flash wt=0
@move_f f=リザードマン@ face=0 out=c wt=0
@qk

@move_f f=effect/集中線@01 face=0 out=c t=500 wt=1

@se f=床にドサッ
@qk

@se f=歩く_近
@move_f f=リザードマン@蔑み_z face=0 body=-1 in=c t=2000

@bs f=リザードマン@屈辱_頬_z body=-1 face=0
[Lizard Girl/rmn0060]「Hem! No matter how many times, I will drain you up.」

@fade type=out t=250 wt=1

@move_f f=リザードマン@ out=c t=20 face=0 wt=0

@bs
@se
@bgm
@bgs
@bgv
@rps

@fade type=in t=20 wt=1

;;============================
