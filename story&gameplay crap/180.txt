;;============================
;;※○マップ15

;;★BGM　モンスター城
@bgm f=Dungeon3

@bg f=要塞通路
@fade type=in t=500 wt=1

[Rick]「…um?」

;;★SE　香り
@se f=Up3
@wait t=100
 There's a sweet smell in the air.

 I have not noticed and keep running, but a feeling of upset arises in my heart.

;;★BGM停止
@bgm

@move_f f=サキュバス@普通 body=-1 face=0 in=c t=300
[Succubus/skb0001]「It's seldom to see humans」

@bs
@bg f=要塞通路
@fade type=in t=500 wt=1

;;★BGM　モンスター登場
@bgm f=Scene2

 I feel more upset when I see her.

[Rick]「Are you…a monster girl」

;;★動かす
@bs f=サキュバス@誘惑 body=-1 face=0 x=c
@move f=サキュバス@誘惑 x=0 y=0 w=100 h=100 t=150 wt=1
@move f=サキュバス@誘惑 x=0 y=50 w=100 h=100 t=120 wt=1
@move f=サキュバス@誘惑 x=0 y=0 w=100 h=100 t=150 wt=1
[Succubus/skb0002]「Hoho…what do you think I am?」

@bs
@bg f=要塞通路
@fade type=in t=500 wt=1

 Blue skins and horns, and there's a tail too…looks like devil.

 But, the fragrance from this monster girl would make human dumb.

[Rick]「Are you…Succubus?」

@move_f f=サキュバス@普通 body=-1 face=0 in=c t=300
[Succubus/skb0003]「Bingo. I'll give you a good present」

;;★白フラ
@flash col=255,255,255,255 t=300
[Rick]「I don't have time to do that!」

@bs f=サキュバス@誘惑 body=-1 face=0
[Succubus/skb0004]「You have plenty of time. And…are you sleepy with my fragrance?」

@bs
@bg f=要塞通路
@fade type=in t=500 wt=1

 I indeed feel sleepy after her saying. Did she question to hold off time…

;;★ZOOM
@bs f=サキュバス@普通_z body=-1 face=0 x=c
@move f=サキュバス@普通_z x=0 y=0 w=500 h=500 t=300 wt=1
@bs
@bg f=要塞通路
@fade type=in t=500 wt=1

 Succubus gets close to me and puffs sweet smell.

[Rick]「Ah...」

@move_f f=サキュバス@普通_z face=0 out=l t=150

;;★SE　倒れる　ｑｋ
;;★暗転
@bgf- bg=A-black t=500
@se f=床にドサッ
@qk

 I feel sleepy and fall down.

@bs
@se
@bgm
@bgs
@bgv
@rps

;;============================
