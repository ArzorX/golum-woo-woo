;;============================
;;------------------------------------------------------
;;戦闘2～5回目　勝利
;;------------------------------------------------------

;;★BGM　勝利
@bgm f=Battle5

@bg f=街道
@fade type=in t=500 wt=1

;;★剣で切り裂く演出
@se f=斬る音 pitch=90
@move_f f=effect/斬る@00 face=0 in=c t=100 wt=1
@move_f f=effect/斬る@01 face=0 in=c t=100 wt=1

;;★赤フラ　ｑｋ
@qk
@flash col=255,0,0,255 t=300
@wait t=100
@move_f f=effect/斬る@01 face=0 out=l t=150

@move_f f=リザードマン@焦り_z face=0 body=-1_z in=c t=300
[Lizard Girl/rmn0061]「AHH!?」

@move_f f=リザードマン@焦り_z face=0 out=l t=150

;;★SE　倒れる　ｑｋ
@se f=床にドサッ
@qk

 I cut off her long and thick tail and beat her.

 Lizard Girl can't keep balance because of losing her tail. She can't exert her strength--

 Now Lizard Girl can only lie on the ground pathetically.

@ev f=街道
@bs f=集中線 face=0 body=-1 opa=0

@se f=走る_遠
@bs f=white face=0 body=-1 opa=0 x=0 y=0
@move f=white x=0 y=0 w=100 h=100 opa=0 t=20 wt=1

@fade type=in t=300 wt=0
@move f=集中線 x=0 y=0 w=100 h=100 opa=50 t=2000 wt=0
@move f=white x=0 y=0  w=100 h=100 opa=255 t=2000 wt=0

@move f=森 x=0 y=19  w=105 h=105 opa=255 t=200 wt=1
@move f=森 x=0 y=-19 w=110 h=110 opa=255 t=200 wt=1

@move f=森 x=0 y=19  w=115 h=115 opa=255 t=200 wt=1
@move f=森 x=0 y=-19 w=120 h=120 opa=255 t=200 wt=1

@move f=森 x=0 y=19  w=125 h=125 opa=255 t=200 wt=1
@move f=森 x=0 y=-19 w=130 h=130 opa=255 t=200 wt=1

@move f=森 x=0 y=19  w=135 h=135 opa=255 t=200 wt=1
@move f=森 x=0 y=-19 w=140 h=140 opa=255 t=200 wt=1

@move f=森 x=0 y=19  w=145 h=145 opa=255 t=200 wt=1
@move f=森 x=0 y=0   w=150 h=150 opa=255 t=200 wt=1

[Rick]「Hoah, hoah, hoah!」

 I breathe hastily and leave that place.

@move_f f=リザードマン@屈辱 face=0 body=-1 in=c t=300
[Lizard Girl/rmn0062]「He is only a human......ah......」

@move_f f=リザードマン@屈辱 face=0 out=l t=150

 Lizard Girl stares at my receding figure hatefully.

;;★ｑｋ
@qk
[Rick]「Ah!?」

;;★SE　走り去る
@se f=走る_遠
@wait t=100
 I hear her lament with great pressure. I run away until the sword disappears from my hand.

;;★暗転
;;★時間経過
@fade type=out t=1000 wt=1
@wait t=1000
@fade type=in t=1000 wt=1

[Rick]「Hoah, hoah, I get rid of her......」

 Now my knees trembles and my feet lose consciousness.

 I go ahead totteringly.

@bs
@se
@bgm
@bgs
@bgv
@rps

;;============================
