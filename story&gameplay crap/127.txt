;;============================
;;★BGM　モンスターフィールド
@bgm f=Dungeon10

@bg f=異世界森
@fade type=in t=500 wt=1

[Rick](Here…is the world of the  monster girls?)

 The monster girls that I have never seen are everywhere.

[Rick](Seems they don't mean to attack me…that's good…)

 Many times I meet with monster girls walking to me…

 But they don't attack me.

 We're alert and keep distance each other, and they disappear in the woods.

 I have been attacked until now.

[Rick](The  monster girls living in this world don't like to fight…)

 Maybe not all the monster girls like to fight.

 On another hand--

 There're also many monster girls interested in me, peeping in a crack far away.

[Rick](I'll fight to the end if they attack!)

 I tell myself not to let down guard and walk to the woods.

@bs
@se
@bgm
@bgs
@bgv
@rps

;;============================

