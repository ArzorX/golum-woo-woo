;;============================

;;背景■崖周辺

@bgm f=004

@ev f=森
@bs f=集中線 face=0 opa=0 body=-1

@se f=走る_遠
@fade type=in t=2000 wt=0
@move f=集中線 x=0 y=0  w=100 h=100 opa=70 t=2000 wt=0

@move f=森 x=0 y=19  w=105 h=105 opa=255 t=200 wt=1
@move f=森 x=0 y=-19 w=110 h=110 opa=255 t=200 wt=1

@move f=森 x=0 y=19  w=115 h=115 opa=255 t=200 wt=1
@move f=森 x=0 y=-19 w=120 h=120 opa=255 t=200 wt=1

@move f=森 x=0 y=19  w=125 h=125 opa=255 t=200 wt=1
@move f=森 x=0 y=-19 w=130 h=130 opa=255 t=200 wt=1

@move f=森 x=0 y=19  w=135 h=135 opa=255 t=200 wt=1
@move f=森 x=0 y=-19 w=140 h=140 opa=255 t=200 wt=1

@move f=森 x=0 y=19  w=145 h=145 opa=255 t=200 wt=1
@move f=森 x=0 y=0   w=150 h=150 opa=255 t=200 wt=1
@move f=集中線 x=0 y=0  w=100 h=100 opa=0 t=100 wt=0

[Rick]「 Ha! Ha! Ha! Ha!」

@mes_win back=1 pos=1
\CL  I am running on the rugged path.

\CL  The sense of adversity is behind me. \n Huge fear is holding my breath.
@mes_win back=0 pos=2

@move f=森 x=256 y=0 w=150 h=150 opa=255 t=250 wt=0
@fade type=out t=100 wt=1

@bs
@ev f=森
@bs f=ハーピー@普通 face=0 body=-1
@bs f=集中線 face=0 body=-1 opa=0
@move f=森 x=0 y=0 w=150 h=150 opa=255 t=20 wt=0
@move f=ハーピー@普通 x=0 y=200 w=80 h=80 opa=0 t=20 wt=1
@fade type=in t=300 wt=1

@se f=Wind1
@move f=集中線 x=0 y=0 w=100 h=100 opa=50 t=100 wt=0
@move f=ハーピー@普通 x=0 y=50 w=100 h=100 opa=255 t=150 wt=1
@qk t=500

@se f=「ドクンッ」という心音

@flash t=300 wt=0
@qk

[Rick](It's gonna get me.......! I will be raped to death!)

@mes_win back=1 pos=1
\CL  I peep at the monster girl \n who is chasing after me--

\CL  I can hardly move my feet in great despair.

\CL  Monster girl smiles evilly as if she enjoys the joy of hunting.
@mes_win back=0 pos=2

@bs f=ハーピー@無邪気な笑み y=50 face=0 body=-1
@move f=ハーピー@無邪気な笑み x=0 y=0 w=100 h=100 t=150 wt=1
@move f=ハーピー@無邪気な笑み x=0 y=50 w=100 h=100 t=120 wt=1
[Habbie/mhp0001]「Hehehe, I will get you if you don't run away.」

@bs f=ハーピー@普通_頬 y=50 face=0 body=-1
[Habbie/mhp0002]「I have just had a great meal~Though I'm not hungry now......」

@bs f=ハーピー@無邪気な笑み_頬 y=50 face=0 body=-1
[Habbie/mhp0003]「Hehehe, you have no way to escape after being found by me」

@se f=「ドクンッ」という心音
@qk

[Rick](Alas! I can't run anymore......but......if caught, I will be killed......)

;;@move f=モンスターＣ x=-256 y=0 w=100 h=100 opa=255 t=100 wt=0
;;@move f=森 x=-256 y=0 w=150 h=150 opa=255 t=100 wt=0
@fade type=out t=100 wt=1
@move_f f=ハーピー@ face=0 out=c t=20 wt=0
@bs
@ev f=A-black
@fade type=in t=20 wt=1

@mes_win back=1 pos=1
\CL For monster girls, humans--

@bs f=cutin_rp02_sepia face=0 body=-1 opa=0 x=50
@move f=cutin_rp02_sepia x=0 y=0 w=100 h=100 opa=255 t=300 wt=1

@mes_win back=1 pos=2
\CL are just food at the bottom of the ecological chain.

\CL I have heard about this for many times.

@fade type=out t=250 wt=1

@mes_win back=1 pos=1
\CL The fact that not being able to escape and......\n what will happen after being caught.

\CL However, even I know it before......
@mes_win back=0 pos=2

@bs

;;@ev f=森
;;@bs f=集中線 face=0 body=-1 opa=0

;;@se f=走る_遠
;;@fade type=in t=300 wt=0
;;@move f=集中線 x=0 y=0  w=100 h=100 opa=70 t=1200 wt=0

;;@move f=森 x=0 y=19  w=105 h=105 opa=255 t=200 wt=1
;;@move f=森 x=0 y=-19 w=110 h=110 opa=255 t=200 wt=1

;;@move f=森 x=0 y=19  w=115 h=115 opa=255 t=200 wt=1
;;@move f=森 x=0 y=-19 w=120 h=120 opa=255 t=200 wt=1

;;@move f=森 x=0 y=19  w=125 h=125 opa=255 t=200 wt=1
;;@move f=森 x=0 y=0   w=130 h=130 opa=255 t=200 wt=1

;;@move f=集中線 x=0 y=0  w=100 h=100 opa=0 t=250 wt=0
;;[リック]「はっ！　はっ！　ひっ！　はぁっ！」

;;　一縷の望みを託し、必死に駆けていく。

;;@move f=森 x=256 y=0 w=150 h=150 opa=255 t=100 wt=0
;;@fade type=out t=100 wt=1

@bs
@ev f=森
@bs f=ハーピー@普通 y=50 body=-1 face=0
@bs f=集中線 face=0 body=-1 opa=70
@move f=ハーピー@普通 x=0 y=50 w=100 h=100 opa=255 t=20 wt=0
@move f=森 x=0 y=0 w=150 h=150 opa=255 t=20 wt=1
@fade type=in t=300 wt=1

@bs f=ハーピー@無邪気な笑み y=50 face=0 body=-1
@move f=ハーピー@無邪気な笑み x=0 y=0 w=100 h=100 t=150 wt=1
@move f=ハーピー@無邪気な笑み x=0 y=50 w=100 h=100 t=120 wt=1
[Habbie/mhp0004]「Hehehe, I'm tired of chasing~ It's time to invade you.」

[Rick](Are you kidding! Noooo! I'm gonna die!)

@move f=ハーピー@無邪気な笑み x=-256 y=0 w=100 h=100 opa=255 t=100 wt=0
@move f=森 x=-256 y=0 w=150 h=150 opa=255 t=100 wt=0
@fade type=out t=100 wt=1

@move_f f=ハーピー@ face=0 out=c t=20 wt=0
@bs

@ev f=森
@bs f=集中線 face=0 body=-1 opa=0
@bs f=空 face=0 body=-1 opa=0

@se f=走る_遠
@bs f=white face=0 body=-1 opa=0 x=0 y=0
@move f=white x=0 y=0 w=100 h=100 opa=0 t=20 wt=1

@fade type=in t=300 wt=0
@move f=集中線 x=0 y=0 w=100 h=100 opa=50 t=2000 wt=0
@move f=white x=0 y=0  w=100 h=100 opa=255 t=2000 wt=0

@move f=森 x=0 y=19  w=105 h=105 opa=255 t=200 wt=1
@move f=森 x=0 y=-19 w=110 h=110 opa=255 t=200 wt=1

@move f=森 x=0 y=19  w=115 h=115 opa=255 t=200 wt=1
@move f=森 x=0 y=-19 w=120 h=120 opa=255 t=200 wt=1

@move f=森 x=0 y=19  w=125 h=125 opa=255 t=200 wt=1
@move f=森 x=0 y=-19 w=130 h=130 opa=255 t=200 wt=1

@move f=森 x=0 y=19  w=135 h=135 opa=255 t=200 wt=1
@move f=森 x=0 y=-19 w=140 h=140 opa=255 t=200 wt=1

@move f=森 x=0 y=19  w=145 h=145 opa=255 t=200 wt=1
@move f=森 x=0 y=0   w=150 h=150 opa=255 t=200 wt=1

@bgm
@se f=風切り音2
@move f=空 x=0 y=0 w=120 h=120 opa=255 t=250 wt=1

@move f=空 x=0 y=76 w=120 h=120 opa=255 t=5000 wt=0
@move f=white x=0 y=0 w=100 h=100 opa=0 t=300 wt=1

@se f=「ドクンッ」という心音

[Rick]「Ah......」

@bgs f=Wind pitch=60

@move f=空 x=0 y=-192 w=150 h=150 opa=255 t=100 wt=0
@fade type=out t=100 wt=1

@mes_win back=1 pos=1
\CL  My fear towards death comes to a climax when I close my eyes......

@bs
@ev f=崖
@bs f=集中線 face=0 body=-1 opa=50
@fade type=in t=100 wt=0

\CL  I lose balance with my feet.

@mes_win back=0 pos=2

[Rick]「It happens......」

@mes_win back=1 pos=1

\CL  I fall down with a huge noise.

\CL  Time seems to stop. \n My falling down is like a slow motion.

@mes_win back=0 pos=2

[Rick](Am I gonna......die......)

@move f=集中線 x=0 y=0 w=120 h=120 opa=0 t=250 wt=0
@move f=崖 x=256 y=-192 w=150 h=150 opa=255 t=500 wt=0
@fade type=out t=100 wt=1

@bgs
@mes_win back=1 pos=1

\CL  The instant when I think of this, I lose consciousness and plunge into darkness.

@mes_win back=0 pos=2

@bs
;;背景■暗転

;;効果■時間経過
@se f=Earth3
@wait t=500

@rps
@wait t=2000

;;============================
