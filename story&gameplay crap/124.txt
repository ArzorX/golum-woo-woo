;;============================
;;------------------------------------------------------
;;戦闘2～5回目　勝利
;;------------------------------------------------------

;;★BGM　勝利
@fade type=out t=20 wt=1

@move_f f=マンティコア@ face=0 body=-1 out=c t=20 wt=0
@bs wt=0

@ev f=砂漠遺跡 t=20 wt=0
@bs f=effect/攻撃開始@3 body=-1 face=0 opa=120 t=20 wt=0
@bs f=マンティコア@笑み_z body=-1 face=0 t=20 wt=0

@move f=砂漠遺跡 x=0 y=0 w=120 h=120 opa=255 t=20 wt=0
@move f=effect/攻撃開始@3 x=0 y=0 w=120 h=120 opa=120 t=20 wt=0

@fade type=in t=20 wt=1

@bgm f=006 pitch=110

@se f=Blow4
@flash t=500 wt=0
@qk t=600 wt=1

@bs f=マンティコア@蔑み_z body=-1 face=0 t=20 wt=0

@se f=締め付ける
@move f=マンティコア@蔑み x=10 y=0 w=100 h=100 opa=255 t=50 wt=1
@move f=マンティコア@蔑み x=-10 y=0 w=100 h=100 opa=255 t=50 wt=1
@move f=マンティコア@蔑み x=10 y=0 w=100 h=100 opa=255 t=50 wt=1
@move f=マンティコア@蔑み x=0 y=0 w=100 h=100 opa=255 t=50 wt=1

@se f=風切り音1
@flash t=1000 wt=0
@move_f f=マンティコア@ face=0 body=-1 out=c t=20 wt=0

@wait t=250

@se f=風切り音2
@move_f f=effect/effect5@02 face=0 in=c t=50 wt=1

@se f=Sword2
@move_f f=effect/打撃@03_1 face=0 in=c t=50 wt=1

@flash t=300 wt=0
@move_f f=effect/effect2@13 face=0 in=c t=100 wt=1
@move_f f=effect/effect5@ face=0 out=c t=20 wt=1
@qk

@move_f f=effect/effect2@ face=0 out=c t=20 wt=1
@move_f f=effect/打撃@02_2 face=0 in=c t=20 wt=1

@se f=Sword2
@move_f f=effect/effect2@22 face=0 in=c t=50 wt=1
@qk

@se f=Blow4
@move_f f=effect/打撃@04 face=0 in=c t=250 wt=1
@qk t=500

@flash t=300 wt=0
@move_f f=effect/打撃@ face=0 out=c t=20 wt=0

@se f=風切り音1
@move_f f=effect/引っ叩く@04_2 face=0 in=c t=50 wt=1

@se f=風切り音2
@move_f f=effect/引っ叩く01@02 face=0 in=c t=20 wt=1

@move_f f=effect/打撃@02_2 face=0 in=c t=20 wt=1
@se f=Sword1
@move_f f=effect/effect2@ face=0 out=c t=20 wt=0
@move_f f=effect/打撃@03_3 face=0 in=c t=50 wt=1
@flash t=300 wt=0
@qk

@se f=Blow4
@flash t=500 wt=0
@move_f f=effect/打撃@ face=0 out=c t=20 wt=0
@move_f f=effect/引っ叩く01@ face=0 out=c t=20 wt=0
@move_f f=effect/引っ叩く@ face=0 out=c t=20 wt=1

@move_f f=マンティコア@蔑み body=-1 face=0 in=c t=100
@move f=マンティコア@蔑み x=10 y=0 w=100 h=100 opa=255 t=50 wt=1
@move f=マンティコア@蔑み x=-10 y=0 w=100 h=100 opa=255 t=50 wt=1
@move f=マンティコア@蔑み x=10 y=0 w=100 h=100 opa=255 t=50 wt=1
@move f=マンティコア@蔑み x=0 y=0 w=100 h=100 opa=255 t=50 wt=1

@wait t=250

@bs f=マンティコア@屈辱 body=-1 face=0
[Manticore/mtc0113]「Not died...」

 Manticore is surprised when she see I resurrect time and time again.

@bs f=マンティコア@睨み body=-1 face=0
[Manticore/mtc0114]「I don't know it's so strong…ordinary humans' soul would be already to the limit...」

 Now the pressure I felt just now has moved from Manticore.

 Probably it's because I have more power than before…

;;★白フラ　ｑｋ

[Rick]「Haha!」

@se f=風切り音2
@bs f=マンティコア@蔑み_z body=-1 face=0

@se f=締め付ける
@move f=マンティコア@蔑み x=10 y=0 w=100 h=100 opa=255 t=50 wt=1
@move f=マンティコア@蔑み x=-10 y=0 w=100 h=100 opa=255 t=50 wt=1
@move f=マンティコア@蔑み x=10 y=0 w=100 h=100 opa=255 t=50 wt=1
@move f=マンティコア@蔑み x=0 y=0 w=100 h=100 opa=255 t=50 wt=1

@se f=風切り音1
@flash t=1000 wt=0
@move_f f=マンティコア@ face=0 body=-1 out=c t=20 wt=0

@wait t=250

@se f=風切り音2
@move_f f=effect/effect5@02 face=0 in=c t=50 wt=1

@se f=Sword2
@move_f f=effect/打撃@03_1 face=0 in=c t=50 wt=1

@flash t=300 wt=0
@move_f f=effect/effect2@13 face=0 in=c t=100 wt=1
@move_f f=effect/effect5@ face=0 out=c t=20 wt=1
@qk

@move_f f=effect/effect2@ face=0 out=c t=20 wt=1
@move_f f=effect/打撃@02_2 face=0 in=c t=20 wt=1

@se f=Sword2
@move_f f=effect/effect2@22 face=0 in=c t=50 wt=1
@qk

@se f=Blow4
@move_f f=effect/打撃@04 face=0 in=c t=250 wt=1
@qk t=500

@flash t=300 wt=0
@move_f f=effect/打撃@ face=0 out=c t=20 wt=0

@se f=風切り音1
@move_f f=effect/引っ叩く@04_2 face=0 in=c t=50 wt=1

@se f=風切り音2
@move_f f=effect/引っ叩く01@02 face=0 in=c t=20 wt=1

@move_f f=effect/打撃@02_2 face=0 in=c t=20 wt=1
@se f=Sword1
@move_f f=effect/effect2@ face=0 out=c t=20 wt=0
@move_f f=effect/打撃@03_3 face=0 in=c t=50 wt=1
@flash t=300 wt=0
@qk

@se f=Blow4
@flash t=500 wt=0
@fade type=out t=500 wt=1
@move_f f=effect/打撃@ face=0 out=c t=20 wt=0
@move_f f=effect/引っ叩く01@ face=0 out=c t=20 wt=0
@move_f f=effect/引っ叩く@ face=0 out=c t=20 wt=1

;;背景■暗転

;;効果■時間経過
@move_f f=マンティコア@ face=0 body=-1 out=c t=20 wt=0

@bs
@se
@bgm
@bgs
@bgv
@rps

@fade type=in t=20 wt=1

;;============================
