;;============================
;;★BGM　モンスター城
@bgm f=Dungeon3

@bg f=要塞通路
@fade type=in t=500 wt=1

@mes_win back=1 pos=1
[The·Orte/olt0011]「Finally, I got here」
@mes_win back=0 pos=2

 I come to the door of the upper floor--

 There's the voice of The·Orte.

 Orte floats in front of me.

[Rick]「As you said…I'm now going to fight Nyack. Let me go」

@bs f=ジ・オルテ@微笑 body=-1 face=0 x=c
@move f=ジ・オルテ@微笑 x=0 y=0 w=100 h=100 t=150 wt=1
@move f=ジ・オルテ@微笑 x=0 y=50 w=100 h=100 t=120 wt=1
@move f=ジ・オルテ@微笑 x=0 y=0 w=100 h=100 t=150 wt=1
[The·Orte/olt0012]「Sorry. I lied to you」

[Rick]「...What do you mean?」

@bs f=ジ・オルテ@普通 body=-1 face=0
[The·Orte/olt0013]「Because I want you feel hopeless」

@bs f=ジ・オルテ@笑み body=-1 face=0
[The·Orte/olt0014]「I want you sorrowing in desperation...」

[Rick]「Then you failed」

@bs f=ジ・オルテ@怒り body=-1 face=0 x=c
@move f=ジ・オルテ@怒り x=0 y=0 w=100 h=100 t=150 wt=1
@move f=ジ・オルテ@怒り x=0 y=50 w=100 h=100 t=120 wt=1
@move f=ジ・オルテ@怒り x=0 y=0 w=100 h=100 t=150 wt=1
[The·Orte/olt0015]「No. I have not failed」

@bs
@bg f=要塞通路
@fade type=in t=500 wt=1

 Orte shakes her head.

[Rick]「Are we…foes!?」

;;★BGM停止
@bgm

;;★ZOOM
@bs f=ジ・オルテ@屈辱 body=-1 face=0 x=c
@move f=ジ・オルテ@屈辱 x=0 y=600 w=500 h=500 t=300 wt=1
@bs
@bg f=要塞通路
@fade type=in t=500 wt=1

[The·Orte/olt0016]「I don't hate you. But…you're a human. I hate humans!」

@bs
@bg f=要塞通路
@fade type=in t=500 wt=1

;;★BGM　ボス登場
@bgm f=Battle6

;;★白フラ　ｑｋ
@qk
@flash col=255,255,255,255 t=300
[Rick]「Ah!?」

 Orte rushes to me with eyes full of hatred.

@move_f f=ジ・オルテ@屈辱 face=0 out=l t=150

;;★白フラ　ｑｋ
@qk
@flash col=255,255,255,255 t=300
[Rick]「Ha!?」

 I dodge her attack and jump away to keep distance.

[Rick](You can't win…)

 I feel the overwhelming gap of power. I'm trembling with fear.

 Even this, I can't be defeated by desperation.

@bs
@se
@bgm
@bgs
@bgv
@rps

;;============================
