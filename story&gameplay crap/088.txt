;;============================
;;★BGM　ピンチ
@bgm f=Scene1

@bg f=荒野
@fade type=in t=300 wt=1
@wait t=200

@bg f=街道
@fade type=in t=300 wt=1
@wait t=200

@bg f=街門
@fade type=in t=300 wt=1
@wait t=300

[Rick](The horse is broken......!?)

 I can hear the lament of residents in the city.

[Rick]「Take the residents to somewhere safe. I'll handle these monster girls.」

;;★動かす
@bs f=村人Ｂ face=0 body=-1 x=l
@move f=村人Ｂ x=0 y=50 w=100 h=100 t=150 wt=1
@move f=村人Ｂ x=0 y=0 w=100 h=100 t=120 wt=1

[Soldier]「Yes, yes!」

@se f=走る_遠
@move_f f=村人Ｂ face=0 out=r t=200

@bs
@bg f=街門
@fade type=in t=500 wt=1

 I hold the sword tightly and break into the city.

@fadeout t=1000 wt=0
@wait t=2000

@bs
@se
@bgm
@bgs
@bgv
@rps

;;============================
