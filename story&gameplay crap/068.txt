;;============================
;;★BGM　ピンチ
@bgm
@fade type=out t=1200 wt=0

@ev f=湖
@se f=走る_遠

@fade type=in t=1200 wt=0

@move f=湖 x=0 y=19  w=105 h=105 opa=255 t=200 wt=1
@move f=湖 x=0 y=-19 w=110 h=110 opa=255 t=200 wt=1
@move f=湖 x=0 y=19  w=115 h=115 opa=255 t=200 wt=1
@move f=湖 x=0 y=-19 w=120 h=120 opa=255 t=200 wt=1
@move f=湖 x=0 y=19  w=125 h=125 opa=255 t=200 wt=1
@move f=湖 x=0 y=0 w=130 h=130 opa=255 t=200 wt=1

 I am hurried to the lake.

@bgm f=003

[Rick]「Appear again......in the surrounding......)

;;★SE　剣登場
@se f=Sword4
@move_f f=effect/剣 face=0 in=c t=300 wt=1

 I hold the sword tightly.

 There is no doubt that monster girl is in the neighborhood.

@move_f f=effect/剣 face=0 out=c t=300

@se f=剣構え01

[Rick](Now, I must fight!)

 I want to flee, but I can't let monster girls commit crimes here.

;;　ゴクッと生唾を飲み下すと、モンスター娘の討伐にとりかかる。

;;　近くにいる筈のモンスター娘を探し、慎重に歩いて行く。

;;★白フラ　ｑｋ
@se f=歩く_遠 l=1
@qk

[Rick]「!!?」

 A creature comes close to me. It doesn't look like a human.

[Rick](Monster girl!?)

@se
@bgs
@se f=剣構え01

 I slash that thing hastily.

@se f=歩く_遠
@move_f f=ドッペルゲンガー@元 face=0 in=c t=300 wt=1

[Rick]「Uh......!?」

@se f=触手がうごめく音
@move f=ドッペルゲンガー@元 x=10 y=0 w=100 h=100 opa=255 t=50 wt=1
@move f=ドッペルゲンガー@元 x=-10 y=0 w=100 h=100 opa=255 t=50 wt=1
@move f=ドッペルゲンガー@元 x=10 y=0 w=100 h=100 opa=255 t=50 wt=1
@move f=ドッペルゲンガー@元 x=0 y=0 w=100 h=100 opa=255 t=50 wt=1

 The look of the strange creature becomes twisted--

@move f=ドッペルゲンガー@元 x=0 y=-38 w=110 h=110 opa=255 t=150 wt=1
@move f=ドッペルゲンガー@元 x=0 y=192 w=50 h=50 opa=0 t=120 wt=1

;;@move_f f=自警団Ａ face=0 in=c t=300

[Rick]「It's, it's me.....?」

 It changes to my shape and stands before me.

@flash t=300
@qk

[Rick]「Ah!?」

 But only an instant, it becomes me.

;;★SE　ぐにゃぐにゃ
@se f=触手がうごめく音

 The breasts of that creature swells and her face becomes more attracting.

;;★BMG停止
@bgm

 It's just like a female me--

;;★BGM　モンスター登場
@bgm f=004

@move_f f=ドッペルゲンガー@普通 body=-1 face=0 in=c t=300
[Double Shadow/dpg0006]「Hello, are you the Hero?」

 She asks me in a light voice of a girl.

[Rick]「They, they call me this way......」

;;★ZOOM
@bs f=ドッペルゲンガー@微笑 body=-1 face=0 x=c

[Double Shadow/dpg0007]「Really. Then have a try. Let's begin!」

@se f=風切り音2
@bs f=ドッペルゲンガー@微笑_z body=-1 face=0 x=c


;;★白フラ　ｑｋ

@flash col=255,255,255,255 t=300 wt=0
@qk

[Rick]「Woah!? Woah!?」


;;============================
