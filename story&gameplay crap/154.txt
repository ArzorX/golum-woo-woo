;;============================

;;★BGM　モンスター登場
@bgm f=Scene2

@bg f=墓場
@fade type=in t=500 wt=1

 After a short time, I resurrect.

@move_f f=ファントム@焦り body=-1 face=0 in=c t=300
[Phantom/ftm0048]「What? Ah? Ah!?」

@bs
@bg f=墓場
@fade type=in t=500 wt=1

 Phantom is surprised by my resurrection and leaves.

[Rick](Ok, I can move…!)

 Maybe her surprise unties me, I regain freedom.

@bs
@se
@bgm
@bgs
@bgv
@rps

;;============================
