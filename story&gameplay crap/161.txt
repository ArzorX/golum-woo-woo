;;============================
;;※ここで調べ物をすると『自警団の資料10』が手に入ります。
;;※以下『自警団の資料10』を手に入れた時のテキストです。

;;★BGM　古民家
@bgm f=Field5

@bg f=古民家
@fade type=in t=500 wt=1

[Rick]「Anybody is here…?」

 Was there a person like me brought to this world ever lived here?

 I search and find a diary.

[Rick]「...」

 Is this the owner's diary?

;;★SE　ページめくる
@se f=Book1
@wait t=100
 I open it.

;;★BGM停止
@bgm
@fade type=out t=1000 wt=1
@wait t=1000
@fade type=in t=1000 wt=1

;;効果■時間経過
;;★BGM　回想(シリアス)
@bgm f=Dungeon5

[Rick]「Wow…If what's in the diary is true...」

;;★SE　本を置く
@se f=湯のみを置く
@wait t=100
 I put it back after reading.

 A girl once called Holy Virgin fell in love with a man.

 But, if the story of Holy Virgin loving a man spreads in followers--

 She would be in a risk.

 Knowing this, the man leaves…

 But Holy Virgin was unable to suppress her feeling.

 She's worrying about if she's abandoned--

 And decided to look for the man.

 The man knew that and greeted her with smile…

 The followers wished Holy Virgin never be stained--

 They killed Holy Virgin in front of the man.

 Enduring severe pain on the back, Holy Virgin watched--

 The man greeting with smile…

 Holy Virgin thought it's because the man betrayed her and finally lost her consciousness with anger.

 Holy Virgin's body was brought to the Order.

 The man could do nothing but suffering alone.

 Since that…

 Rumors about Holy Virgin spread.

 Setting off the forbidden Black Magic in order to keep her soul--

 And attaching her soul on monsters. Rumors like this spread in all ways.

 The man felt heartbroken whenever he heard the rumors--

 But he could do nothing.

 Time passed…

 The man heard about the rumor of Orte attacking humans.

 And her features were totally the same with dead Holy Virgin…

[Rick](Is the man the father of the first leader of Self Vigilante…

 Wanted to save Orte.

 But the old man could do nothing.

 The diary full of regret ends here.

[Rick]「Puff」

 I sigh deeply.

[Rick](If there's anything I can do…)

 Thinking of this, I stand up slowly.

;;※以上ここまでが『自警団の資料10』を手に入れた時のテキストです。

@bs
@se
@bgm
@bgs
@bgv
@rps

;;============================
