;;============================
;;★BGM　街
@bgm f=bgm01

@fade type=out t=20 wt=1

@ev f=街
@bs f=自警団Ａ face=0 body=-1 y=50

@move f=街 x=0 y=0 w=120 h=120 opa=255 t=20 wt=1

@se f=歩く_遠
@fade type=in t=1000 wt=1

[Soldier]「Come here.」

@move_f f=自警団Ａ face=0 out=c t=250 wt=1
;;　兵士に連れて来られたのは、一つも窓の無い殺風景な部屋だった。

[Rick]「Ah......」

@bs f=母親 x=-90 y=50 opa=0 body=-1 face=0
@bs f=娘 x=90 y=50 opa=0 body=-1 face=0

@move f=母親 x=-90 y=50 w=100 h=100 t=500 wt=0
@move f=娘 x=90 y=50 w=100 h=100 t=500 wt=1

@move f=母親 x=-90 y=100 w=100 h=100 t=120 wt=1
@move f=母親 x=-90 y=50 w=100 h=100 t=150 wt=1

[Mother]「Ohoh, it's the Hero sama!」

@move f=娘 x=90 y=0 w=100 h=100 t=100 wt=1
@move f=娘 x=90 y=50 w=100 h=100 t=80 wt=1
@move f=娘 x=90 y=0 w=100 h=100 t=100 wt=1
@move f=娘 x=90 y=50 w=100 h=100 t=80 wt=1

[Daughter]「Hero brother!」

 I see the mother and the daughter I saved from the monster girl.

@move f=母親 x=-90 y=50 w=100 h=100 t=500 opa=0 wt=0
@move f=娘 x=90 y=50 w=100 h=100 t=500 opa=0  wt=1

@bs f=村長 x=0 y=50 opa=0 body=-1 face=0
@move f=村長 x=0 y=50 w=100 h=100 t=500 wt=0

 The man beside them should be the chief of this city.

@move f=村長 x=0 y=50 w=100 h=100 opa=0 t=500 wt=1

@move f=母親 x=-90 y=50 w=100 h=100 t=500 wt=0
@move f=娘 x=90 y=50 w=100 h=100 t=500 wt=1

@move f=母親 x=-90 y=100 w=100 h=100 t=120 wt=1
@move f=母親 x=-90 y=50 w=100 h=100 t=150 wt=1

[Mother]「Thank you. Owing to you, my daughter and I came back to the city safely.」

@move f=母親 x=-90 y=50 w=100 h=100 t=500 opa=0 wt=0
@move f=娘 x=90 y=50 w=100 h=100 t=500 opa=0  wt=1

@move f=村長 x=0 y=50 w=100 h=100 t=500 wt=1

@move f=村長 x=0 y=100 w=100 h=100 t=120 wt=1
@move f=村長 x=0 y=50 w=100 h=100 t=150 wt=1

[Morris]「Hero sama, thanks for saving my people.」

;;★動かす
@move f=村長 x=0 y=100 w=100 h=100 t=120 wt=1
@move f=村長 x=0 y=50 w=100 h=100 t=150 wt=1

[Morris]「I'm Morris, the chief of the city.」

[Rick]「No, no, I......well......I'm not Hero......」

[Morris]「You are so modest......I've been waiting for you.」

;;★動かす
@move f=村長 x=0 y=100 w=100 h=100 t=120 wt=1
@move f=村長 x=0 y=50 w=100 h=100 t=150 wt=1

[Morris]「Please save this city......No, please save the world for all humans.」

@move f=村長 x=0 y=50 w=100 h=100 opa=0 t=500 wt=1

@move f=母親 x=-90 y=50 w=100 h=100 t=500 wt=0
@move f=娘 x=90 y=50 w=100 h=100 t=500 wt=1

 Morris, mother and daughter bow to me.

[Rick]「No, no, I'm not......」

@move f=娘 x=90 y=0 w=100 h=100 t=100 wt=1
@move f=娘 x=90 y=50 w=100 h=100 t=80 wt=1
@move f=娘 x=90 y=0 w=100 h=100 t=100 wt=1
@move f=娘 x=90 y=50 w=100 h=100 t=80 wt=1

[Daughter]「Hero brother, please save us!」

@qk

[Rick]「Woo......」

 They stare at me hopefully.

@se f=締め付ける
@qk

[Rick]「......I will do my best......」

 I see Morris, mother and the little girl begging me--

 I can only make a promise reluctantly.

@move f=母親 x=-90 y=50 w=100 h=100 t=500 opa=0 wt=0
@move f=娘 x=90 y=50 w=100 h=100 t=500 opa=0  wt=1

@move f=村長 x=0 y=50 w=100 h=100 t=500 wt=1

@move f=村長 x=0 y=0 w=100 h=100 t=150 wt=1
@move f=村長 x=0 y=50 w=100 h=100 t=120 wt=1

[Morris]「Ohoh! I'm grateful to you!」

[Rick]「No, well, I'm not that strong......like you think......」

[Morris]「You are so modest. I will arrange accommodation for you, please wait a minute.」

;;★歩き去る
@se f=歩く_遠
@move f=村長 x=0 y=50 w=100 h=100 opa=0 t=500 wt=1

;;　顔を輝かせたモリスが早足に部屋を出て行く。

[Rick](Though it's good to have a place to live......what would they want from me......?)

 Upset towards the future fills in my brain.

;;背景■暗転

;;効果■時間経過

@fade type=out t=1000 wt=1

@bs
@se
@bgm
@bgs
@bgv
@rps

@fade type=in t=20 wt=1

;;============================
