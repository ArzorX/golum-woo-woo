;;============================
;;★BGM　ピンチ
@bgm f=Scene1

@bg f=海周辺
@fade type=in t=500 wt=1

[Rick]「This is...」

 It's as the gatekeeper said--

 Fossilized birds and animals are everywhere.

 And the fossilized fish pushed to the shore by spindrift.

 In the face of such strange scene, I feel a chill goes up behind. 

;;★白フラ
@flash col=255,255,255,255 t=300
[Rick]「Ah!?」

 Moving on for a while, I find more human statues 

[Rick]「Are these the soldiers…for investigation?」

 There're more chills when I see their screwy expressions.

[Rick]「No doubt that's made by monster girls...」

 I hold the sword tightly and look around--

;;★白フラ　ｑｋ
;;★SE　ドスン
@se f=「ドンッ」と突き飛ばす音
@qk
@flash col=255,255,255,255 t=300
 Knock knock!

 There's the sound of something heavy falling on the ground.

;;★白フラ　ｑｋ
@qk
@flash col=255,255,255,255 t=300
[Rick]「Oh!」

 The flying bird was fossilized and fell down.

[Rick]「Where!? Where're you!?」

 I'm holding my sword and shout.

;;★BGM停止
@bgm

@move_f f=石化蛇娘@妖艶 body=-1 face=0 in=c t=300
[Gorgon/glg0024]「You're brave enough to come here」

@bs
@bg f=海周辺
@fade type=in t=500 wt=1

;;★BGM　モンスター登場
@bgm f=Scene2

 There's something heavy slips close to me--

 The monster girl appears.

[Rick](She's the type I've never seen)

@move_f f=石化蛇娘@普通 body=-1 face=0 in=c t=300
[Gorgon/glg0025]「I thought you'd run as you heard the fossilization legend」

;;★動かす
@bs f=石化蛇娘@蔑み body=-1 face=0 x=c
@move f=石化蛇娘@蔑み x=0 y=50 w=100 h=100 t=150 wt=1
@move f=石化蛇娘@蔑み x=0 y=0 w=100 h=100 t=120 wt=1

[Gorgon/glg0026]「What's up? Anything changed? Coward」

[Rick]「You know nothing about me! You, don't know me!」

;;★動かす
@bs f=石化蛇娘@笑み body=-1 face=0 x=c
@move f=石化蛇娘@笑み x=0 y=0 w=100 h=100 t=150 wt=1
@move f=石化蛇娘@笑み x=0 y=50 w=100 h=100 t=120 wt=1
@move f=石化蛇娘@笑み x=0 y=0 w=100 h=100 t=150 wt=1
[Gorgon/glg0027]「Hoho, don't bark. Your legs are shivering」

;;★ｑｋ
@qk
[Rick]「Oh！」

@bs f=石化蛇娘@睨み body=-1 face=0
[Gorgon/glg0028]「don't you hate to be Hero? Why don't you run?」

[Rick]「I…I come to do what I should do. That's it...」

@bs f=石化蛇娘@蔑み body=-1 face=0
[Gorgon/glg0029]「Ha, do you think you're Hero」

@bs f=石化蛇娘@妖艶 body=-1 face=0
[Gorgon/glg0030]「You'll not be suffering if only you give up everything and escape」

[Rick]「Shut, shut up…!」

@bs
@bg f=海周辺
@fade type=in t=500 wt=1

;;★ZOOM
@bs f=石化蛇娘@妖艶 body=-1 face=0 x=c
@move f=石化蛇娘@妖艶 x=0 y=700 w=500 h=500 t=300 wt=1
[Gorgon/glg0031]「You, shut up. I'm Gorgon. Shiver before my power!」

@move_f f=石化蛇娘@妖艶 face=0 out=l t=150
@bs
@se
@bgm
@bgs
@bgv
@rps

;;============================
