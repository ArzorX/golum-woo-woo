;;============================
;;以下合流のテキストです。
;;★BGM　シリアス

@fade type=out t=20 wt=1

@bgm
;;★SE　歩く

@ev f=廃館
@move f=廃館 x=0 y=0 w=100 h=100 opa=255 t=20 wt=1

@bgm f=005 pitch=60

@se f=歩く（革靴・アスファルト）
@fade type=in t=2000 wt=1

;;　周囲に気を配りながら慎重に、建物の中を歩いて行く。

[Rick]「That is.....?」

 In the shabby house where everything is obsolete--

@move f=廃館 x=-150 y=130 w=150 h=150 opa=255 t=1000 wt=1

@move_f f=絵画娘@普通 body=-1 face=0 in=c t=300

 There is a beautifully decorated portrait.

[Rick](This painting......is so great......the portrait speaks.)

 This is an ancient well-preserved painting in a shabby building.

 But the woman in the painting looks so attractive and vivid.

 I come close to the painting as if I am seduced by it.

@move_f f=絵画娘@普通 body=-1 face=0 out=c t=300

@se f=歩く_遠
@move f=廃館 x=-160 y=140 w=160 h=160 opa=255 t=1000 wt=1

@move_f f=絵画娘@普通_z face=0 in=c t=1000
@bgm

[Painting/odr0001]「Hehe.」

@se f=「ドクンッ」という心音
@flash t=300 wt=0
@qk

@bgm f=003

[Rick]「Uh!?」

 I didn't expect the woman in the painting gives a smile.

@se f=風切り音2
@flash col=255,255,255,255 t=300 wt=0
;;★白フラ　ｑｋ
@qk

@bgm f=004

[Rick]「AHH!?」

@se f=締め付ける
@qk t=500

 Suddenly, two hands reach out of the painting.

@se f=締め付ける
@qk t=500

[Monster Girl?/odr0002]「Gotcha♪」


;;============================
