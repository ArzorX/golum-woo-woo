;;============================

@fade type=out t=20 wt=1

@move_f f=ナイア_真@ face=0 out=c t=20 wt=0
@bs wt=0

@ev f=要塞内部 t=20 wt=0
@bs f=effect/攻撃開始@5 body=-1 face=0 opa=120 t=20 wt=0
@bs f=ナイア_真@笑み body=-1 face=0 t=20 wt=0

@move f=要塞内部 x=0 y=0 w=120 h=120 opa=255 t=20 wt=0
@move f=effect/攻撃開始@5 x=0 y=0 w=120 h=120 opa=120 t=20 wt=0

@fade type=in t=20 wt=1

;;★BGM　ボス登場
@bgm f=006 pitch=110

@bs f=ナイア_真@普通_z body=-1 face=0 t=20 wt=0

@se f=締め付ける
@move f=ナイア_真@普通_z x=10 y=0 w=100 h=100 opa=255 t=50 wt=1
@move f=ナイア_真@普通_z x=-10 y=0 w=100 h=100 opa=255 t=50 wt=1
@move f=ナイア_真@普通_z x=10 y=0 w=100 h=100 opa=255 t=50 wt=1
@move f=ナイア_真@普通_z x=0 y=0 w=100 h=100 opa=255 t=50 wt=1

@se f=風切り音1
@flash t=1000 wt=0
@move f=ナイア_真@普通_z x=0 y=0 w=100 h=100 opa=0 t=20 wt=1

@bs f=effect/衝突@01 face=0 body=-1 opa=0 wt=0
@move f=effect/衝突@01 x=0 y=0 w=120 h=120 opa=0 t=20 wt=1

@bgm
@se f=爆発4
@se f=Sword2
@move f=effect/衝突@01 x=0 y=0 w=120 h=120 opa=255 t=50 wt=1
@qk t=1000

@move f=ナイア_真@普通_z x=0 y=0 w=100 h=100 opa=255 t=20 wt=1
@move f=effect/衝突@01 x=0 y=0 w=120 h=120 opa=0 t=500 wt=1

@bs f=ナイア_真@普通 body=-1 face=0 t=20 wt=0

[Rick](It's…impossible…)

 I still have energy.

 But I have a setback before I lose all my energy.

@move_f f=ナイア_真@蔑み body=-1 face=0 in=c t=300
[Nyack/nia0066]「Hoho, well done. But, that's all」

@se f=Fire1
@move f=effect/攻撃開始@5 x=0 y=0 w=300 h=300 opa=0 t=1000 wt=1

@bgm f=003

;;★SE　歩く近づく
@se f=歩く_近
@bs f=ナイア_真@蔑み_z body=-1 face=0

 Nyack walks to me.

@fade type=out t=250 wt=1

@move_f f=ナイア_真@ face=0 out=c t=150

@bs
@se
@bgm
@bgs
@bgv
@rps

@fade type=in t=20 wt=1

;;============================
