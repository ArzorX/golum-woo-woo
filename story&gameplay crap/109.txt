;;============================
;;------------------------------------------------------
;;戦闘2～5回目　勝利
;;------------------------------------------------------

@bg f=海周辺
@fade type=in t=500 wt=1

;;★BGM　勝利
@bgm f=Battle5

@move_f f=石化蛇娘@焦り body=-1 face=0 in=c t=300
[Gorgon/glg0077]「Hoho…can you resurrect no matter how many times, …I can't make it.」

[Rick]「Want to surrender?」

@bs f=石化蛇娘@妖艶 body=-1 face=0
[Gorgon/glg0078]「Surrender? People would not forgive me even we surrender. Right?」

[Rick]「Of course...」

@bs
@bg f=海周辺
@fade type=in t=500 wt=1

 Recalling the anger and hate to monster girls, I can't find any words to respond.

@move_f f=石化蛇娘@睨み body=-1 face=0 in=c t=300
[Gorgon/glg0079]「Hero…Although I lose out to you, I'm not defeated by humans」

@bs f=石化蛇娘@蔑み body=-1 face=0
[Gorgon/glg0080]「Humans can't resurrect endlessly」

[Rick]「...」

;;★動かす
@bs f=石化蛇娘@普通 body=-1 face=0 x=c
@move f=石化蛇娘@普通 x=0 y=300 w=200 h=200 t=300 wt=1
@wait t=100
[Gorgon/glg0081]「And…is it good if you die too many times?」

@bs
@bg f=海周辺
@fade type=in t=500 wt=1

[Rick]「…do you know this sword?」

;;★BGM停止
@bgm

@bs f=石化蛇娘@屈辱 body=-1 face=0 y=0
[Gorgon/glg0083]「…Manticore.」
@wait t=200

;;★BGM　シリアス
@bgm f=bgm03

[Rick]「Manticore?」

@bs f=石化蛇娘@焦り body=-1 face=0
[Gorgon/glg0083]「The horrible creatures with plural biological status… She has more power than me」

@bs f=石化蛇娘@妖艶 body=-1 face=0
[Gorgon/glg0084]「She maybe know the details. Anyway…hoho」

[Rick]「Where is Manticore? How to defeat her?」

;;★動かす
@bs f=石化蛇娘@普通 body=-1 face=0 x=c
@move f=石化蛇娘@普通 x=0 y=50 w=100 h=100 t=150 wt=1
@move f=石化蛇娘@普通 x=0 y=0 w=100 h=100 t=120 wt=1
[Gorgon/glg0085]「Hum, I don't have to tell you so much. Go to find it yourself」

[Rick]「Then…I'll let you say out with my strength」

@bs f=石化蛇娘@蔑み body=-1 face=0
[Gorgon/glg0086]「I don't play unprepared fight. Originally, I don't expect to win. That's why I choose here.」
@move_f f=石化蛇娘@蔑み out=l t=300

[Rick]「Ah!? You…!?」

@bs
@bg f=海周辺
@fade type=in t=500 wt=1

;;★SE　水ばしゃ
@se f=Water1
@wait t=100
 The monster girl runs to the sea shore and jumps into the sea.

[Rick]「Ah!?」

[Gorgon/glg0087]「You humans are weak. You can't fly in the air neither swim in the sea. Just tremble on the land.」
@se f=Water1
@move_f f=石化蛇娘@笑み out=r t=300

[Rick]「...」


 I can only helplessly watch her escaping.

 Now I have no one to ask about info of Manticore.

;;★白フラ　ｑｋ
;;★SE　石びきびき
@se f=Battle2
@qk
@flash col=255,255,255,255 t=300
[Soldier]「Oh…ah!? Ah!」

 The fossilization of soldiers and animals are relived after the monster girl left.

[Rick](It's paralysis more than curse…)

[Rick](After all, I know our enemy. Manticore…)

@move_f f=村人Ｂ face=0 in=l t=300 wt=1
[Soldier]「Lord Hero…we...」

[Rick]「The monster girl will not come for a while. I'm going to leave right now」

[Rick]「Maybe we already found the murderer that made the world into chaos. Please tell others.」

;;★動かす
@bs f=村人Ｂ face=0 body=-1
@move f=村人Ｂ x=0 y=50 w=100 h=100 t=150 wt=1
@move f=村人Ｂ x=0 y=0 w=100 h=100 t=120 wt=1
[Soldier]「Lord Hero, please be careful! We're waiting for you to come back!」

@move_f f=村人Ｂ face=0 out=c t=100

 The soldiers salute. I'm on my way for looking for Manticore.

@bs
@se
@bgm
@bgs
@bgv
@rps

;;============================
