;;============================
;;★BGM　モンスターフィールド
@bgm f=Dungeon10

@bg f=異世界森
@fade type=in t=500 wt=1

;;★SE　香り
@se f=Up3
@wait t=100
[Rick]「What on earth…is this smell...」

;;★SE　歩く
@se f=歩く（革靴・アスファルト）
@wait t=100
 I follow the sweet scent and walk toward it.

[Rick](There're more strange plants…)

 Can it be the scent is from these plants.

 I keep walking--

;;★BGM停止
@bgm

;;★ZOOM
@bs f=アルラウネ@微笑 body=-1 face=0 x=c

[Alruane/alr0001]「Wow, rare visitor～. Anyway, catch you first♪  Hey!」

;;★BGM　モンスター登場
@bgm f=Scene1

 The plant-type monster girl appears, waving cirrus.

;;★白フラ
@flash col=255,255,255,255 t=300
[Rick]「Oh! …are you a monster girl!」

@bs
@se
@bgm
@bgs
@bgv
@rps

;;============================
