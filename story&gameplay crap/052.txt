;;============================
;;--------------------------------------------------------------------
;;戦闘イベント：1回目（敗北確定）
;;--------------------------------------------------------------------

;;★BGM　敗北

@fade type=out t=20 wt=1
@move_f f=絵画娘@ face=0 out=c t=20 wt=1
@bs wt=0
@ev f=廃館
@move f=廃館 x=-160 y=140 w=160 h=160 opa=255 t=1000 wt=1
@bs f=絵画娘@普通_z body=-1 face=0
@fade type=in t=20 wt=1

@bgm

@se f=風切り音2
@flash t=300

@wait t=250

;;★ｑｋ
@se f=締め付ける
@qk

@wait t=250

@se f=「ドクンッ」という心音
@qk

[Rick]「Why!? Why I can't get rid of them!?」

@bgm f=003

 I am caught by the hands from the portrait.

;;★ZOOM
[Odalisque/odr0003]「Come on, the banquet is about to begin.」

;;★ｑｋ
@qk
@se f=締め付ける

[Rick]「AHHHH!」

@se f=締め付ける
@qk

@wait t=250

@se f=風切り音2
@fade type=out t=300 wt=0
@qk

 I am pressed on the painting tightly by the hands.

 I can't control my body.

@move_f f=絵画娘@ face=0 out=c t=20 wt=1

@bs
@se
@bgm
@bgs
@bgv
@rps

@fade type=in t=20 wt=1

;;============================
