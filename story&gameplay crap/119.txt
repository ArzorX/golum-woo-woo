;;============================
;;--------------------------------------------------------------------
;;戦闘イベント：1回目（敗北確定）
;;--------------------------------------------------------------------

;;★BGM　敗北

@fade type=out t=20 wt=1

@move_f f=マンティコア@ face=0 body=-1 out=c t=20 wt=0
@bs wt=0

@ev f=砂漠遺跡 t=20 wt=0
@bs f=effect/攻撃開始@3 body=-1 face=0 opa=120 t=20 wt=0
@bs f=マンティコア@笑み_z body=-1 face=0 t=20 wt=0

@move f=砂漠遺跡 x=0 y=0 w=120 h=120 opa=255 t=20 wt=0
@move f=effect/攻撃開始@3 x=0 y=0 w=120 h=120 opa=120 t=20 wt=0

@fade type=in t=20 wt=1

@bgm f=006 pitch=110

@se f=Blow4
@flash t=500 wt=0
@qk t=600 wt=1

@bs f=マンティコア@蔑み_z body=-1 face=0 t=20 wt=0

@se f=締め付ける
@move f=マンティコア@蔑み x=10 y=0 w=100 h=100 opa=255 t=50 wt=1
@move f=マンティコア@蔑み x=-10 y=0 w=100 h=100 opa=255 t=50 wt=1
@move f=マンティコア@蔑み x=10 y=0 w=100 h=100 opa=255 t=50 wt=1
@move f=マンティコア@蔑み x=0 y=0 w=100 h=100 opa=255 t=50 wt=1

@se f=風切り音1
@flash t=1000 wt=0
@move_f f=マンティコア@ face=0 body=-1 out=c t=20 wt=0

@wait t=250

@se f=風切り音2
@move_f f=effect/effect5@02 face=0 in=c t=50 wt=1

@se f=Sword2
@move_f f=effect/打撃@03_1 face=0 in=c t=50 wt=1

@flash t=300 wt=0
@move_f f=effect/effect2@13 face=0 in=c t=100 wt=1
@move_f f=effect/effect5@ face=0 out=c t=20 wt=1
@qk

@move_f f=effect/effect2@ face=0 out=c t=20 wt=1
@move_f f=effect/打撃@02_2 face=0 in=c t=20 wt=1

@se f=Sword2
@move_f f=effect/effect2@22 face=0 in=c t=50 wt=1
@qk

@se f=Blow4
@move_f f=effect/打撃@04 face=0 in=c t=250 wt=1
@qk t=500

@flash t=300 wt=0
@move_f f=effect/打撃@ face=0 out=c t=20 wt=0

@se f=風切り音1
@move_f f=effect/引っ叩く@04_2 face=0 in=c t=50 wt=1

@se f=風切り音2
@move_f f=effect/引っ叩く01@02 face=0 in=c t=20 wt=1

@move_f f=effect/打撃@02_2 face=0 in=c t=20 wt=1
@se f=Sword1
@move_f f=effect/effect2@ face=0 out=c t=20 wt=0
@move_f f=effect/打撃@03_3 face=0 in=c t=50 wt=1
@flash t=300 wt=0
@qk

@se f=Blow4
@flash t=500 wt=0
@move_f f=effect/打撃@ face=0 out=c t=20 wt=0
@move_f f=effect/引っ叩く01@ face=0 out=c t=20 wt=0
@move_f f=effect/引っ叩く@ face=0 out=c t=20 wt=1

@move_f f=マンティコア@蔑み body=-1 face=0 in=c t=100
@move f=マンティコア@蔑み x=10 y=0 w=100 h=100 opa=255 t=50 wt=1
@move f=マンティコア@蔑み x=-10 y=0 w=100 h=100 opa=255 t=50 wt=1
@move f=マンティコア@蔑み x=10 y=0 w=100 h=100 opa=255 t=50 wt=1
@move f=マンティコア@蔑み x=0 y=0 w=100 h=100 opa=255 t=50 wt=1

@wait t=250

@bs f=マンティコア@睨み body=-1 face=0
[Manticore/mtc0015]「You idiot…! How could humans defeat me!」

;;★白フラ　ｑｋ
@qk
@flash col=255,255,255,255 t=300

[Manticore/mtc0016]「Haha!」

@bs f=effect/引っ叩く@04_2 face=0 body=-1 opa=0 wt=0

@move f=砂漠遺跡 x=30 y=-76 w=120 h=120 opa=255 t=250 wt=0
@move f=マンティコア@蔑み x=30 y=50 w=95 h=95 opa=255 t=250 wt=1

@se f=Ice9 pitch=140
@move f=マンティコア@蔑み x=0 y=50 w=100 h=100 opa=255 t=50 wt=1

@move f=effect/引っ叩く@04_2 x=0 y=0 w=100 h=100 opa=255 t=100 wt=0
@move f=マンティコア@蔑み x=120 y=-76 w=120 h=120 opa=255 t=100 wt=1

@se f=風切り音2 pitch=60
@move f=砂漠遺跡 x=-102 y=70 w=120 h=120 opa=255 t=100 wt=0
@move f=マンティコア@蔑み x=500 y=-384 w=200 h=200 opa=0 t=100 wt=0
@move f=effect/引っ叩く@04_2 x=100 y=0 w=100 h=100 opa=0 t=100 wt=1

@wait t=100

@bs f=effect/衝突@01 face=0 body=-1 opa=0 wt=0
@move f=effect/衝突@01 x=0 y=0 w=120 h=120 opa=0 t=20 wt=1

@se f=爆発4
@se f=Sword2
@move f=effect/衝突@01 x=0 y=0 w=120 h=120 opa=255 t=50 wt=1
@qk t=1000

[Rick]「Whoops!」

 Manticore pushes off the ground and rushes to me.

 Her long sharp claws blows by in the air.

 I hit back with my sword.

 But…

@bgm

@se f=風切り音2
@flash t=500

@se f=刺す
@flash col=255,0,0,255 t=300 wt=0
@qk t=500

[Rick]「Ah, ahh!?」

@se f=潰れる
@flash col=255,0,0,255 t=300 wt=0
@qk t=500

@move f=砂漠遺跡 x=0 y=0 w=120 h=120 opa=255 t=20 wt=0
@move f=マンティコア@蔑み x=0 y=0 w=100 h=100 opa=255 t=20 wt=1
@move f=effect/衝突@01 x=0 y=0 w=120 h=120 opa=0 t=200 wt=1

;;　ガキンッ！

;;　サソリの尻尾がブンッと空を薙ぐと――

;;　腹部に衝撃が伝わってきた。

;;★ｑｋ
@se f=締め付ける
@qk

[Rick]「Ah, ahh, ha, ha, ha...」

@bgm f=003

@bs f=マンティコア@睨み body=-1 face=0
[Manticore/mtc0017]「Just so so」

 In the face of such unequal contest--

 I'm not afraid of it.

[Rick](…Other monster girls will also believe I can resurrect and they don't know how strong I am. Is that the original energy of Fine Sword?)

 And I have never felt my soul has been decreased. Is there anything that different from what Manticore said.)

[Rick](…so)

@se f=締め付ける
@qk

[Rick]「Just, fight...」

@se f=Fire1
@move f=effect/攻撃開始@3 x=0 y=0 w=300 h=300 opa=0 t=1000 wt=1

@bs f=マンティコア@微笑 body=-1 face=0
[Manticore/mtc0018]「Seems I can absorb a lot of soul」

[Rick]「What're you going to do...」

@se f=歩く_遠
@move_f f=マンティコア@ face=0 out=c t=1000 wt=1
@move_f f=マンティコア@微笑_z face=0 in=c t=1500 wt=1

@bs f=マンティコア@笑み_頬_z body=-1 face=0
[Manticore/mtc0019]「You know it? To do what you have been done」

;;★白フラ　ｑｋ
[Rick]「Ah!?」

@fade type=out t=1000 wt=1

@move_f f=マンティコア@ face=0 out=c t=20 wt=1
@bs
@se
@bgm
@bgs
@bgv
@rps

@fade type=in t=20 wt=1

;;============================
