;;============================

[Rick]「Um~ Go to sleep again......」

@se f=衣擦れの音1
@fade type=out t=1000 wt=1

 I don't resist sleepness and fall asleep soon.


@wait t=3000

;;背景■宿

;;★BGM　ピンチ

@se f=走る_近
@wait t=3000

@bs f=自警団Ａ face=0 body=-1 y=50

@se f=Open4
@fade type=in t=100 wt=0
@qk t=500

;;★ｑｋ

@move f=自警団Ａ x=0 y=0 w=100 h=100 t=150 wt=1
@move f=自警団Ａ x=0 y=50 w=100 h=100 t=120 wt=1

[Soldier]「 Hero sama, Hero sama!」

[Rick]「Woo......What's wrong?」

@bgm f=003

;;★白フラ
@move f=自警団Ａ x=0 y=0 w=100 h=100 t=150 wt=1
@move f=自警団Ａ x=0 y=50 w=100 h=100 t=120 wt=1

[Soldier]「Monster girls appear......!! We're going to defend them, Hero sama, please come with us!」

[Rick]「Ah, woo, um......」

@se f=衣擦れの音1
@fade type=out t=500 wt=1

 I rub my eyes to become sober and get up from the bed.

@bs
@se
@bgm
@bgs
@bgv
@rps

@fade type=in t=20 wt=1

;;============================
