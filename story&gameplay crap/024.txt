;;============================
;;※『自警団の資料１』を手に入れた時の状態です。
;;★BGM　古民家

;;★SE　本ばさっ
@se f=床にドサッ
@qk

@wait t=500

@se f=衣擦れの音1
@move_f f=本 face=0 in=c t=300 wt=1

[Rick]「Um? What's this......」

 There are many books on the table. I open up one of them.

[Rick]「What's on it......dairy of the house owner......? It looks very old......」

@move_f f=本 face=0 out=c t=300 wt=1

;;★BGM停止
@bgm

[Rick]「History of Self Vigilante......? What's Self Vigilante?」

;;★BGM　回想(シリアス)
@bgm f=003

;;マップが映るのを回避したい。現状は対策浮かばず。バッファで対応？

;;★SE　ページめくる
@se f=Book1
@bs f=本 face=0 body=-1

 I'm interested in this book and read it carefully.

[Rick]「In order to defend the alien girls, people founded...... Self Vigilante?」

@se f=Book1

[Rick]「These alien girls are monster girls, right......」

[Rick]「People used to fight monster girls......uh?」

@se f=Book1

[Rick]「There were monster girls before?」

[Rick]「I thought monster girls were just legends made up by adults......」

@se f=Book1
@move_f f=本 face=0 out=c t=300 wt=1

 But, now monster girls indeed invade our world.

 Chivalric Order were eliminated in a second......

@se f=Book1
@bs f=本 face=0 body=-1

[Rick]「It wrote Self Vigilante defeated monster girls and forced them to go back to their world.」

 Long time ago, monster girls invaded the world just like now--

 Thanks to Self Vigilante, monster girls all went back to their world.

@bgm

@se f=Book1
@move_f f=本 face=0 out=c t=300 wt=1

[Rick](If only there were powerful heroes like members of Self Vigilante......)

 Maybe the world has gone back to peace.

[Rick]「If someone could manage it before, maybe......now there will be people who could defeat monster girls......」

[Rick]「Did this house owner write a biography for Self Vigilante......?」

[Rick]「To be exact, the house owner is the offspring of the person who wrote a biography.」

[Rick]「Let me have a look at another book to find information of Self Vigilante......」

;;============================
