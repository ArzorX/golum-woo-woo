;;============================
;;------------------------------------------------------
;;戦闘2～5回目　敗北
;;------------------------------------------------------

;;★BGM　敗北
@bgm

@fade type=out t=20 wt=1
@bs
@bg f=森
@bs f=ゴブリン娘@焦り body=-1 face=0
@fade type=in t=20 wt=1

@se f=Blow4

@flash t=500 wt=0
@qk t=500 wt=1

@bgm f=003

@bs f=ゴブリン娘@普通 body=-1 face=0
[Goblin BOSS/gbr0037]Hehe♪ You're just as weak as I thought. You're no threat.

@bs f=ゴブリン娘@笑み_頬 body=-1 face=0

[Goblin BOSS/gbr0038]Ora! Erect again I see, we'll quickly wring your semen out!

@move_f f=ゴブリン娘@ face=0 out=c t=300 wt=1

;;★ゴブリン雑魚　数人ズーム
@bs f=ゴブリンＡ face=0 body=-1 opa=0 x=-80 t=20
@bs f=ゴブリンＢ face=0 body=-1 opa=0 x=-100 t=20
@bs f=ゴブリンＣ face=0 body=-1 opa=0 x=-380 t=20
@bs f=ゴブリンＤ face=0 body=-1 opa=0 x=400 t=20

@se f=風切り音2
@move f=ゴブリンＡ x=-80 y=50 w=100 h=100 t=200 opa=255 wt=1
@move f=ゴブリンＡ x=0 y=0 w=300 h=300 t=200 opa=0 wt=1

@se f=風切り音2
@move f=ゴブリンＢ x=-100 y=50 w=100 h=100 opa=255 t=200 wt=1
@move f=ゴブリンＢ x=0 y=0 w=300 h=300 t=200 opa=0 wt=1

@se f=風切り音2
@move f=ゴブリンＣ x=-380 y=50 w=100 h=100 opa=255 t=200 wt=1
@move f=ゴブリンＣ x=0 y=0 w=300 h=300 t=200 opa=0 wt=1

@se f=風切り音2
@move f=ゴブリンＤ x=400 y=50 w=100 h=100 opa=255 t=200 wt=1
@move f=ゴブリンＤ x=0 y=0 w=300 h=300 t=200 opa=0 wt=1

@se f=壁に「ドンッ」
@fade type=out t=100 wt=0

@qk
@se f=床にドサッ

[Having said that, the goblins pounce on me and wring me to death again.]

@bs
@se
@bgm
@bgs
@bgv
@rps

@fade type=in t=20 wt=1

;;============================

