;;============================

;;背景■崖周辺
;;★BGM　シリアス

@fade type=out t=20 wt=1
@bs wt=1 t=20
@wait t=1000
@fade type=in t=20 wt=1

@se f=Flash1
@bs f=A_white face=0 body=-1 t=20 wt=1
@fade type=in t=100 wt=1

@fade type=out t=200 wt=1
@fade type=in t=200 wt=1

@fade type=out t=200 wt=1
@fade type=in t=200 wt=1

@se f=Flash3
@move_f f=A_white face=0 out=c t=2000 wt=1

@wait t=1000
@fade type=out t=20 wt=1

@bgm f=005 pitch=60

@bs f=空 face=0 body=-1 opa=0
@bs f=white face=0 body=-1
@bs f=eyelid_u face=0 body=-1
@bs f=eyelid_b face=0 body=-1

@move f=空 x=0 y=76 w=120 h=120 opa=255 t=20 wt=0
@move f=white x=0 y=0 w=120 h=120 opa=255 t=20 wt=1

@fade type=in t=300 wt=0
@move f=eyelid_u x=0 y=-384 h=100 w=100 opa=0 t=1500 wt=0
@move f=eyelid_b x=0 y=384 h=100 w=100 opa=0 t=1500 wt=0
@move f=white x=0 y=0 h=100 w=100 opa=0 t=1500 wt=1

[リック]（俺……死んだんじゃないのか……？）

　不意に意識が戻ってくると、ボンヤリと宙を見つめる。

@se f=衣擦れの音1
@move f=空 x=0 y=-70 w=120 h=120 opa=255 t=100 wt=0
@fade type=out t=100 wt=1
@bs wt=0

@ev f=崖周辺
@move f=崖周辺 x=0 y=-70 w=120 h=120 opa=255 t=20 wt=1

@fade type=in t=100 wt=1

[リック]「死んで……無い？」

　モンスター娘に精液を死ぬまで絞りだされた。

　それなのに、傷一つない状態で俺は身を起していた。

[リック]「な、何でだ……？」

@se f=衣擦れの音1
@move f=崖周辺 x=0 y=0 w=120 h=120 opa=255 t=300 wt=1

@wait t=500
;;　訳が分からず、フラフラと歩き出すと――

@se f=歩く_遠
@move f=崖周辺 x=0 y=19  w=125 h=125 opa=255 t=400 wt=1
@move f=崖周辺 x=0 y=-19 w=130 h=130 opa=255 t=400 wt=1

@fade type=out t=1600 wt=0
@move f=崖周辺 x=0 y=19  w=135 h=135 opa=255 t=400 wt=1
@move f=崖周辺 x=0 y=-19 w=140 h=140 opa=255 t=400 wt=1

@move f=崖周辺 x=0 y=19  w=145 h=145 opa=255 t=400 wt=1
@move f=崖周辺 x=0 y=0   w=150 h=150 opa=255 t=400 wt=1

@wait t=1000

;;★BGM停止
@bgm

@bs wt=0

@ev f=崖周辺
@move f=崖周辺 x=0 y=-70 w=120 h=120 opa=255 t=20 wt=0

@se f=歩く_遠
@bs f=ハーピー@普通 face=0 body=-1
@bs f=集中線 face=0 body=-1 opa=0
@move f=ハーピー@普通 x=0 y=50 h=100 w=100 opa=255 t=20 wt=1
@fade type=in t=500 wt=1

@wait t=500
@se f=「ドクンッ」という心音
@qk

@wait t=500
@move f=ハーピー@普通 x=0 y=0 w=100 h=100 t=150 wt=1
@move f=ハーピー@普通 x=0 y=50 w=100 h=100 t=120 wt=1

[ハーピー/mhp0034]「あれれ？　死んだ筈なのに……なんでなんで？」

;;★BGM　モンスター登場(雑魚)
@bgm f=003

　さっき俺を殺したモンスター娘とまた遭遇してしまう。

　状況を理解できないのは、俺だけじゃなかった。

　死んだ筈の獲物が生き返っている。

　その事に驚き戸惑っているモンスター娘。

[リック]「あっ……」

;;★SE　剣登場
@se f=Sword4
@move_f f=effect/剣 face=0 in=c t=300 wt=1
　消えていた剣が、いつの間にか手に戻ってきている。
@move_f f=effect/剣 face=0 out=c t=300

;;★EF　ZOOM

@bs f=ハーピー@無邪気な笑み face=0 body=-1 y=50
@move f=ハーピー@無邪気な笑み x=0 y=0 w=100 h=100 t=150 wt=1
@move f=ハーピー@無邪気な笑み x=0 y=50 w=100 h=100 t=120 wt=1
[ハーピー/mhp0035]「もう一度……頂戴っ♪」

@bs f=ハーピー@普通 face=0 body=-1 y=50
@move f=ハーピー@普通 x=0 y=50 w=100 h=100 opa=0 t=300 wt=1

@se f=風切り音1
@move f=集中線 x=0 y=0 w=120 h=120 opa=70 t=300 wt=0
@move f=ハーピー@普通 x=0 y=0 w=150 h=150 opa=0 t=20 wt=1
@move f=ハーピー@普通 x=0 y=0 w=152 h=152 t=100 wt=1

;;★EF　QK
@bgm
@se f=「ドクンッ」という心音
@qk

[リック]「ひっ！？」


　今すぐ逃げたい！

　そんな衝動が突き上がってくるけど……。

@se f=締め付ける
@qk

@bgm f=004

[リック]（に、逃げ切れない。逃げても、また殺されるだけだ！）

　逃げ切れない事は、もう分かっている。

@move f=ハーピー@普通 x=0 y=0 w=152 h=152 opa=0 t=200 wt=1
@se f=Sword4
@move_f f=effect/剣 face=0 in=c t=300 wt=1

　助かりたければ戦うしかない。

@move_f f=effect/剣 face=0 out=c t=300

@move f=集中線 x=0 y=0 w=120 h=120 opa=0 t=100 wt=0
@move f=ハーピー@普通 x=100 y=-230 w=160 h=160 opa=255 t=250 wt=1

[リック]（足を……足を狙えば……）

　鋭い爪のついている足を剣で切断する事が出来れば……。

　俺にも勝機はあるかもしれない。

;;　羽で覆われた体よりも剥き出しの脚の方が、ダメージを与えられそうだった。

@move f=集中線 x=0 y=0 w=120 h=120 opa=70 t=100 wt=0
@move f=ハーピー@普通 x=0 y=0 w=155 h=155 opa=255 t=250 wt=1

　迫って来る威圧感に慄きながらも――

[リック]「っっっ！」

;;★SE　切り裂く
@se f=風切り音2 pitch=60
@bs f=effect/effect2@14 face=0 body=-1
@move f=崖周辺 x=-102 y=70 w=120 h=120 opa=255 t=400 wt=0
@move f=ハーピー@普通 x=-500 y=300 w=200 h=200 opa=0 t=200 wt=0
@move f=effect/effect2@14 x=-100 y=0 w=100 h=100 opa=0 t=200 wt=1

@fade type=out t=100 wt=1
@bs wt=0
@bs f=effect/effect3@08 face=0 body=-1 wt=0
@move f=effect/effect3@08 x=0 y=0 w=120 h=120 opa=0 t=20 wt=1
@fade type=in t=20 wt=0

@se f=斬る音
@move f=effect/effect3@08 x=0 y=0 w=100 h=100 opa=255 t=100 wt=0
@qk t=500 wt=1

　渾身の一撃で、剣を薙ぐ。

;;★EF　赤フラ　ｑｋ
@fade type=out t=20 wt=1
@bs wt=0

@ev f=崖周辺
@bs f=ハーピー@驚き face=0 body=-1 opa=0

@move f=崖周辺 x=0 y=-70 w=120 h=120 opa=255 t=20 wt=0
@move f=ハーピー@驚き x=0 y=50 w=100 h=100 opa=0 t=20 wt=1

@fade type=in t=100 wt=1

@move f=ハーピー@驚き x=0 y=50 w=100 h=100 opa=255 t=500 wt=1

@bs f=ハーピー@痛み face=0 body=-1 y=50
@move f=ハーピー@痛み x=10 y=50 w=100 h=100 opa=255 t=50 wt=1
@move f=ハーピー@痛み x=-10 y=50 w=100 h=100 opa=255 t=50 wt=1
@move f=ハーピー@痛み x=10 y=50 w=100 h=100 opa=255 t=50 wt=1
@move f=ハーピー@痛み x=0 y=50 w=100 h=100 opa=255 t=50 wt=1

[ハーピー/mhp0036]「ぐあっ！」

　モンスター娘がよろめき、羽から血がにじんでいる。

;;★EF　ｑｋ
@qk

[リック]「うわっ！　うわっ！　うわぁあっ！」

@bs f=ハーピー@ face=0 body=-1 wt=0
@bs f=ハーピー@驚き face=0 body=-1 y=50

@se f=風切り音1
@move_f f=effect/effect3@03 face=0 in=c t=50
@wait t=100
@move_f f=effect/effect3@03 face=0 out=c t=250

@se f=風切り音1
@move_f f=effect/effect3@02 face=0 in=c t=50
@wait t=100
@move_f f=effect/effect3@02 face=0 out=c t=250

@move f=ハーピー@驚き x=0 y=0 w=100 h=100 t=150 wt=1
@move f=ハーピー@驚き x=0 y=50 w=100 h=100 t=120 wt=1

　たまたま当たった一撃によって、俺の方が優位になっている。

　その勢いのままに剣を振り回していくと――

@bs f=ハーピー@痛み face=0 body=-1 y=50
@move f=ハーピー@痛み x=10 y=50 w=100 h=100 opa=255 t=50 wt=1
@move f=ハーピー@痛み x=-10 y=50 w=100 h=100 opa=255 t=50 wt=1
@move f=ハーピー@痛み x=10 y=50 w=100 h=100 opa=255 t=50 wt=1
@move f=ハーピー@痛み x=0 y=50 w=100 h=100 opa=255 t=50 wt=1

;;★EF　動かす
[ハーピー/mhp0037]「くっ！　なんだこいつっ！」

@se f=締め付ける
@move f=崖周辺 x=0 y=-70 w=120 h=120 opa=255 t=400 wt=0
@move f=ハーピー@痛み x=0 y=100 w=100 h=100 opa=255 t=400 wt=1

@move f=ハーピー@痛み x=10 y=100 w=100 h=100 opa=255 t=50 wt=1
@move f=ハーピー@痛み x=-10 y=100 w=100 h=100 opa=255 t=50 wt=1
@move f=ハーピー@痛み x=10 y=100 w=100 h=100 opa=255 t=50 wt=1
@move f=ハーピー@痛み x=0 y=100 w=100 h=100 opa=255 t=50 wt=1

@wait t=500

@bgm
@se f=Wind1

@move f=崖周辺 x=0 y=70 w=120 h=120 opa=255 t=100 wt=0
@move f=ハーピー@痛み x=0 y=0 w=100 h=100 opa=0 t=100 wt=1
@move_f f=ハーピー@ out=c face=0 t=20 wt=0

　勢いに気圧されたように、モンスター娘が逃げ始める。

;;★BGM停止
@move f=崖周辺 x=0 y=0 w=120 h=120 opa=255 t=1000 wt=1
@bgm f=005 pitch=60

[リック]「や、やった……」

@move f=崖周辺 x=0 y=-70 w=120 h=120 opa=255 t=100 wt=1
@se f=床にドサッ
@qk

　視界からモンスター娘の姿が消えると、腰が抜けたようにその場にへたり込む。

;;★EF　白フラ
@se f=淫唇を開く音
@flash col=255,255,255,255 t=300

[リック]「うわっ！？」

　ヌルリとした感触が手に伝わり、マジマジと手の平を見つめる。

;;★SE　くちゅ
@se f=愛液1

[リック]「精液……？」

　今、へたり込んだ場所は、俺が殺された場所。

　モンスター娘に犯され、嬲り殺された時の事がフラッシュバックする。

[リック]「うぁっ……ぁっ、あぁっ……」

　全身がガクガクと震え、大量の汗がドッと噴き出してくる。

[リック]（俺は……殺された……殺されたのに……生き返った？）

[リック]「そ、そんな事……ある筈無い……」

[リック]（だったら……これは……夢か？　今、夢を見てるって事なのか……？）

　これまでの事は、あまりにも現実離れし過ぎている。

　夢だと思う事が、一番現実的な気がした。

[リック]「でも……夢にしてはリアル過ぎる……」

　肩を上下に揺らし息を乱しながら、汗を拭う。

[リック]「と、とにかく行こう……」

　一度は撃退したモンスター娘が戻ってくるかもしれない。

　次もまた、あんなに上手く撃退出来るとは限らない。

[リック]「あ、あれ？　剣が……剣が無い……」

　さっきまで手から離れなかった剣が、今は、手の中に無い。

　辺りを見回すけど、どこにも剣は無かった。

[リック]「ま、まぁ……夢だから……き、気にしなくて良いよな……」

　夢であるならば……。

　剣が出たり消えたりするのも不思議ではない。

　そう自分に言い聞かせると、急いでこの場を離れる事にした。

@fade type=out t=1000 wt=1
@bs
@se
@bgm
@bgs
@bgv
@rps

;;============================
