;;============================
;;------------------------------------------------------
;;戦闘2～5回目　敗北
;;------------------------------------------------------

;;★BGM　敗北

@fade type=out t=20 wt=1
@move_f f=蜘蛛娘@ face=0 body=-1 out=c t=20
@bs wt=0
@bg f=森
@move_f f=蜘蛛娘@普通 face=0 body=-1 in=c t=300
@fade type=in t=20 wt=1
@bgm

;;★SE　糸みたいなのプシュー
;;★白フラ　ｑｋ

@se f=「プシュー」という噴出音
@flash col=255,255,255,255 t=300 wt=0
@qk t=500

@se f=締め付ける
@flash

@flash t=300 wt=0
@qk t=300 wt=1

@bgm f=003

@bs f=蜘蛛娘@微笑 face=0 body=-1
[アラクネ/arc0069]「別に、弱いのは変わらないのね。もう一度食べてみれば何かわかるかしら」

@fade type=out t=300 wt=1

@move_f f=蜘蛛娘@ face=0 body=-1 out=c t=20

@bs
@se
@bgm
@bgs
@bgv

@fade type=in t=20 wt=1

@rps

;;============================
