;;============================
;;------------------------------------------------------
;;戦闘2～5回目　敗北
;;------------------------------------------------------

;;★BGM　敗北
@bgm f=Scene3

@bg f=街
@fade type=in t=500 wt=1

　チクリッとした痛みが首筋に伝う。

;;★赤フラ　ｑｋ
@qk
@flash col=255,0,0,255 t=300
[リック]「く……っ！」

@move_f f=淫蚊娘@笑み body=-1 face=0 in=c t=300
[モスキーナ/msk0079]「んふっ♪　これでまた私の言う事には逆らえなくなっちゃったでしょ？」

@bs
@bg f=街
@fade type=in t=500 wt=1

　催淫の毒を血液へと流し込まれた。

@move_f f=淫蚊娘@怒り body=-1 face=0 in=c t=300
[モスキーナ/msk0080]「また、タップリ血と精液を搾り取ってあげる。その後で殺してあげるから」

;;★動かす
@bs f=淫蚊娘@普通 body=-1 face=0 x=c
@move f=淫蚊娘@普通 x=150 y=200 w=200 h=200 t=300 wt=1
@wait t=100
[モスキーナ/msk0081]「こっちに来て」

@move_f f=淫蚊娘@普通 face=0 out=l t=150
@bs
@bg f=街
@fade type=in t=500 wt=1

　命じられるとフラフラと淫蚊娘の方へと近づいていく。

@bs
@se
@bgm
@bgs
@bgv
@rps

;;============================
