;;============================
;;★BGM　ボス登場
@fade type=out t=20 wt=1

@move_f f=マンティコア@ face=0 body=-1 out=c t=20 wt=0
@bs wt=0

@ev f=砂漠遺跡 t=20 wt=0
@bs f=effect/攻撃開始@3 body=-1 face=0 opa=120 t=20 wt=0

@move f=砂漠遺跡 x=0 y=0 w=120 h=120 opa=255 t=20 wt=0
@move f=effect/攻撃開始@3 x=0 y=0 w=120 h=120 opa=120 t=20 wt=0

@bgm f=004

@se f=風切り音1
@fade type=in t=50 wt=1
@flash t=1000 wt=1

@se f=風切り音2
@move_f f=effect/effect5@02 face=0 in=c t=50 wt=1

@se f=Sword2
@move_f f=effect/打撃@03_1 face=0 in=c t=50 wt=1

@flash t=300 wt=0
@move_f f=effect/effect2@13 face=0 in=c t=100 wt=1
@move_f f=effect/effect5@ face=0 out=c t=20 wt=1
@qk

@move_f f=effect/effect2@ face=0 out=c t=20 wt=1
@move_f f=effect/打撃@02_2 face=0 in=c t=20 wt=1

@se f=Sword2
@move_f f=effect/effect2@22 face=0 in=c t=50 wt=1
@qk

@se f=Blow4
@move_f f=effect/打撃@04 face=0 in=c t=250 wt=1
@qk t=500

@flash t=300 wt=0
@move_f f=effect/打撃@ face=0 out=c t=20 wt=0

@se f=風切り音1
@move_f f=effect/引っ叩く@04_2 face=0 in=c t=50 wt=1

@se f=風切り音2
@move_f f=effect/引っ叩く01@02 face=0 in=c t=20 wt=1

@move_f f=effect/打撃@02_2 face=0 in=c t=20 wt=1
@se f=Sword1
@move_f f=effect/effect2@ face=0 out=c t=20 wt=0
@move_f f=effect/打撃@03_3 face=0 in=c t=50 wt=1
@flash t=300 wt=0
@qk

@bs f=effect/衝突@01 face=0 body=-1 opa=0 wt=0
@move f=effect/衝突@01 x=0 y=0 w=120 h=120 opa=0 t=20 wt=1

@se f=爆発4
@flash t=500 wt=0
@move f=effect/衝突@01 x=0 y=0 w=120 h=120 opa=255 t=50 wt=0
@move_f f=effect/打撃@ face=0 out=c t=20 wt=0
@move_f f=effect/引っ叩く01@ face=0 out=c t=20 wt=0
@move_f f=effect/引っ叩く@ face=0 out=c t=20 wt=1
@qk t=1000

@move f=effect/衝突@01 x=0 y=0 w=120 h=120 opa=0 t=200 wt=1

@se f=締め付ける
@move_f f=マンティコア@蔑み body=-1 face=0 in=c t=100
@move f=マンティコア@蔑み x=10 y=0 w=100 h=100 opa=255 t=50 wt=1
@move f=マンティコア@蔑み x=-10 y=0 w=100 h=100 opa=255 t=50 wt=1
@move f=マンティコア@蔑み x=10 y=0 w=100 h=100 opa=255 t=50 wt=1
@move f=マンティコア@蔑み x=0 y=0 w=100 h=100 opa=255 t=50 wt=1

@wait t=250

@bs f=マンティコア@屈辱 body=-1 face=0
[マンティコア/mtc0115]「ぐっ……ぐぅぅぅっ……」

[リック]「こ、これで……どうだ……」

　呻きながらよろめくマンティコア。

　だが、油断は出来ない。

@move_f f=マンティコア@微笑 body=-1 face=0 in=c t=300
[マンティコア/mtc0116]「ふふっ、これで終わりだとは思っていないのだろう？」

　数瞬の間があり、マンティコアの楽し気な声が響く。

[リック]「あぁ……」

@bs f=effect/攻撃準備@3 face=0 body=-1 opa=0 t=20 wt=0
@move f=effect/攻撃準備@3 x=0 y=0 w=150 h=150 opa=0 t=20 wt=1

@se f=地鳴りの音
@move f=effect/攻撃準備@3 x=0 y=0 w=110 h=110 opa=255 t=3000 wt=0
@qk t=3000 wt=1

@flash col=255,65,100,255 t=500 wt=0
@qk t=500 wt=1

@se f=Thunder7 pitch=80
@se f=Battle3

@move f=effect/攻撃準備@3 x=0 y=0 w=200 h=200 opa=0 t=100 wt=1

@flash col=255,65,100,255 t=500 wt=0
@qk t=500 wt=1

　立ち上がるマンティコアから放たれる、狂気めいたオーラ。

　またパワーアップをしているのは間違いなかった。

;;★白フラ　ｑｋ

[リック]「ぁあああああっ！」

@bs f=effect/引っ叩く@04_2 face=0 body=-1 opa=0 wt=0

@move f=砂漠遺跡 x=30 y=-76 w=120 h=120 opa=255 t=250 wt=0
@move f=マンティコア@蔑み x=30 y=50 w=95 h=95 opa=255 t=250 wt=1

@se f=Ice9 pitch=140
@move f=マンティコア@蔑み x=0 y=50 w=100 h=100 opa=255 t=50 wt=1

@move f=effect/引っ叩く@04_2 x=0 y=0 w=100 h=100 opa=255 t=100 wt=0
@move f=マンティコア@蔑み x=120 y=-76 w=120 h=120 opa=255 t=100 wt=1

@se f=風切り音2 pitch=60
@move f=砂漠遺跡 x=-102 y=70 w=120 h=120 opa=255 t=100 wt=0
@move f=マンティコア@蔑み x=500 y=-384 w=200 h=200 opa=0 t=100 wt=0
@move f=effect/引っ叩く@04_2 x=100 y=0 w=100 h=100 opa=0 t=100 wt=1

@bs f=effect/衝突@01 face=0 body=-1 opa=0 wt=0
@move f=effect/衝突@01 x=0 y=0 w=120 h=120 opa=0 t=20 wt=1

@se f=爆発4
@se f=Sword2
@move f=effect/衝突@01 x=0 y=0 w=120 h=120 opa=255 t=50 wt=1
@qk t=1000

@fade type=out t=50 wt=1

@move_f f=マンティコア@ face=0 body=-1 out=c t=20 wt=0
@move_f f=衝突@ face=0 body=-1 out=c t=20 wt=0
@bs

@fade type=in t=20 wt=1

@se f=風切り音2 pitch=60
@move_f f=effect/斬る@00 face=0 in=c t=150 wt=1
@move_f f=effect/斬る@00 face=0 out=c t=100 wt=1

@se f=風切り音2 pitch=60
@move_f f=effect/effect3@02 face=0 in=c t=150 wt=1
@move_f f=effect/effect3@02 face=0 out=c t=100 wt=1

@se f=壁に「ドンッ」
@wait t=250

@se f=斬る音
@move_f f=effect/effect3@08 face=0 in=c t=100 wt=1
@qk t=1000 wt=1

@fade type=out t=50 wt=1

@bs wt=0

@ev f=砂漠遺跡 t=20 wt=0
@bs f=effect/攻撃開始@3 body=-1 face=0 opa=120 t=20 wt=0
@bs f=マンティコア@蔑み body=-1 face=0 t=20 wt=0

@move f=砂漠遺跡 x=0 y=0 w=120 h=120 opa=255 t=20 wt=0
@move f=effect/攻撃開始@3 x=0 y=0 w=120 h=120 opa=120 t=20 wt=0

@fade type=in t=50 wt=1

@se f=Blow4
@flash t=300 wt=0
@move_f f=effect/打撃@03_3 face=0 in=c t=200 wt=1

@se f=締め付ける
@move f=マンティコア@蔑み x=10 y=0 w=100 h=100 opa=255 t=50 wt=1
@move f=マンティコア@蔑み x=-10 y=0 w=100 h=100 opa=255 t=50 wt=1
@move f=マンティコア@蔑み x=10 y=0 w=100 h=100 opa=255 t=50 wt=1
@move f=マンティコア@蔑み x=0 y=0 w=100 h=100 opa=255 t=50 wt=1

@move_f f=effect/打撃@03_3 face=0 out=c t=200 wt=1

@bs f=マンティコア@蔑み_z body=-1 face=0 x=c
[マンティコア/mtc0117]「ふんっ！　ヌルいっ！」

@se f=風切り音1
@flash t=1000 wt=0
@move f=マンティコア@蔑み x=0 y=0 w=100 h=100 opa=0 t=20 wt=1

@bs f=effect/衝突@01 face=0 body=-1 opa=0 wt=0
@move f=effect/衝突@01 x=0 y=0 w=120 h=120 opa=0 t=20 wt=1

@se f=爆発4
@se f=Sword2
@move f=effect/衝突@01 x=0 y=0 w=120 h=120 opa=255 t=50 wt=1
@qk t=1000

;;　どちらが先に倒れるか……。

;;★SE　剣を振り回す
;;　根競べのようになりながらも、またマンティコアに向かって剣を振り下ろす。

[リック]「がふっ！？」

@move f=マンティコア@蔑み x=0 y=0 w=100 h=100 opa=255 t=20 wt=1
@move f=effect/衝突@01 x=0 y=0 w=120 h=120 opa=0 t=500 wt=1

;;★ｑｋ
@se f=床にドサッ
@qk

[リック]「ぐっ……くぅっ……うぅぅ」

@bs f=マンティコア@屈辱 body=-1 face=0
[マンティコア/mtc0118]「ちっ……！」

　倒れない俺を見て、マンティコアは忌々しそうに舌打ちをする。

@bs f=マンティコア@蔑み body=-1 face=0

[マンティコア/mtc0119]「なぜだ……なぜまだ死なん！」

[リック]「それはわからない……けど、俺は……お前を倒す！」

　力が漲っているのがハッキリと分かる。

　マンティコアに対する畏怖の念は消えていた。

@bs f=マンティコア@睨み body=-1 face=0
[マンティコア/mtc0120]「……」

　警戒を浮かべるマンティコアが俺との距離を取る。

[リック]「いくぞ……っ！」

;;★ZOOM
@bs f=マンティコア@屈辱_z body=-1 face=0 x=c

[マンティコア/mtc0121]「人間風情が……っ！」

@se f=風切り音2
@bs f=マンティコア@蔑み_z body=-1 face=0

@se f=締め付ける
@move f=マンティコア@蔑み x=10 y=0 w=100 h=100 opa=255 t=50 wt=1
@move f=マンティコア@蔑み x=-10 y=0 w=100 h=100 opa=255 t=50 wt=1
@move f=マンティコア@蔑み x=10 y=0 w=100 h=100 opa=255 t=50 wt=1
@move f=マンティコア@蔑み x=0 y=0 w=100 h=100 opa=255 t=50 wt=1

@se f=風切り音1
@flash t=1000 wt=0
@move_f f=マンティコア@ face=0 body=-1 out=c t=20 wt=0

@wait t=250

@se f=風切り音2
@move_f f=effect/effect5@02 face=0 in=c t=50 wt=1

@se f=Sword2
@move_f f=effect/打撃@03_1 face=0 in=c t=50 wt=1

@flash t=300 wt=0
@move_f f=effect/effect2@13 face=0 in=c t=100 wt=1
@move_f f=effect/effect5@ face=0 out=c t=20 wt=1
@qk

@move_f f=effect/effect2@ face=0 out=c t=20 wt=1
@move_f f=effect/打撃@02_2 face=0 in=c t=20 wt=1

@se f=Sword2
@move_f f=effect/effect2@22 face=0 in=c t=50 wt=1
@qk

@se f=Blow4
@move_f f=effect/打撃@04 face=0 in=c t=250 wt=1
@qk t=500

@flash t=300 wt=0
@move_f f=effect/打撃@ face=0 out=c t=20 wt=0

@se f=風切り音1
@move_f f=effect/引っ叩く@04_2 face=0 in=c t=50 wt=1

@se f=風切り音2
@move_f f=effect/引っ叩く01@02 face=0 in=c t=20 wt=1

@move_f f=effect/打撃@02_2 face=0 in=c t=20 wt=1
@se f=Sword1
@move_f f=effect/effect2@ face=0 out=c t=20 wt=0
@move_f f=effect/打撃@03_3 face=0 in=c t=50 wt=1
@flash t=300 wt=0
@qk

@se f=Blow4
@flash t=500 wt=0
@fade type=out t=500 wt=1
@move_f f=effect/打撃@ face=0 out=c t=20 wt=0
@move_f f=effect/引っ叩く01@ face=0 out=c t=20 wt=0
@move_f f=effect/引っ叩く@ face=0 out=c t=20 wt=1

;;背景■暗転

;;効果■時間経過
@move_f f=マンティコア@ face=0 body=-1 out=c t=20 wt=0

@bs
@se
@bgs
@bgv
@rps

@fade type=in t=20 wt=1

;;　威嚇の咆哮が放たれるとビリビリと空気が震える。

;;　その抵抗を排除し、マンティコアに向かって斬りかかっていった。

@move_f f=マンティコア@ face=0 body=-1 out=c t=20 wt=0
@bs wt=0

@ev f=砂漠遺跡 t=20 wt=0
@bs f=effect/攻撃開始@3 body=-1 face=0 opa=120 t=20 wt=0

@move f=砂漠遺跡 x=0 y=0 w=120 h=120 opa=255 t=20 wt=0
@move f=effect/攻撃開始@3 x=0 y=0 w=120 h=120 opa=120 t=20 wt=0

@se f=風切り音1
@fade type=in t=50 wt=1
@flash t=1000 wt=1

@se f=風切り音2
@move_f f=effect/effect5@02 face=0 in=c t=50 wt=1

@se f=Sword2
@move_f f=effect/打撃@03_1 face=0 in=c t=50 wt=1

@flash t=300 wt=0
@move_f f=effect/effect2@13 face=0 in=c t=100 wt=1
@move_f f=effect/effect5@ face=0 out=c t=20 wt=1
@qk

@move_f f=effect/effect2@ face=0 out=c t=20 wt=1
@move_f f=effect/打撃@02_2 face=0 in=c t=20 wt=1

@se f=Sword2
@move_f f=effect/effect2@22 face=0 in=c t=50 wt=1
@qk

@se f=Blow4
@move_f f=effect/打撃@04 face=0 in=c t=250 wt=1
@qk t=500

@flash t=300 wt=0
@move_f f=effect/打撃@ face=0 out=c t=20 wt=0

@se f=風切り音1
@move_f f=effect/引っ叩く@04_2 face=0 in=c t=50 wt=1

@se f=風切り音2
@move_f f=effect/引っ叩く01@02 face=0 in=c t=20 wt=1

@move_f f=effect/打撃@02_2 face=0 in=c t=20 wt=1
@se f=Sword1
@move_f f=effect/effect2@ face=0 out=c t=20 wt=0
@move_f f=effect/打撃@03_3 face=0 in=c t=50 wt=1
@flash t=300 wt=0
@qk

@bs f=effect/衝突@01 face=0 body=-1 opa=0 wt=0
@move f=effect/衝突@01 x=0 y=0 w=120 h=120 opa=0 t=20 wt=1

@se f=爆発4
@flash t=500 wt=0
@move f=effect/衝突@01 x=0 y=0 w=120 h=120 opa=255 t=50 wt=0
@move_f f=effect/打撃@ face=0 out=c t=20 wt=0
@move_f f=effect/引っ叩く01@ face=0 out=c t=20 wt=0
@move_f f=effect/引っ叩く@ face=0 out=c t=20 wt=1
@qk t=1000

@move f=effect/衝突@01 x=0 y=0 w=120 h=120 opa=0 t=200 wt=1

@se f=締め付ける
@move_f f=マンティコア@蔑み body=-1 face=0 in=c t=100
@move f=マンティコア@蔑み x=10 y=0 w=100 h=100 opa=255 t=50 wt=1
@move f=マンティコア@蔑み x=-10 y=0 w=100 h=100 opa=255 t=50 wt=1
@move f=マンティコア@蔑み x=10 y=0 w=100 h=100 opa=255 t=50 wt=1
@move f=マンティコア@蔑み x=0 y=0 w=100 h=100 opa=255 t=50 wt=1

@bgm
@se f=Fire1
@move f=effect/攻撃開始@3 x=0 y=0 w=300 h=300 opa=0 t=1000 wt=1

@wait t=500

@se f=締め付ける
@bs f=マンティコア@屈辱 body=-1 face=0
@move f=マンティコア@屈辱 x=10 y=0 w=100 h=100 opa=255 t=50 wt=1
@move f=マンティコア@屈辱 x=-10 y=0 w=100 h=100 opa=255 t=50 wt=1
@move f=マンティコア@屈辱 x=10 y=0 w=100 h=100 opa=255 t=50 wt=1
@move f=マンティコア@屈辱 x=0 y=0 w=100 h=100 opa=255 t=50 wt=1

@move f=マンティコア@屈辱 x=0 y=384 w=100 h=100 opa=0 t=200 wt=1

@se f=床にドサッ
@qk

@bgm f=003

[リック]「はっ……くっ……はぁ、はぁ」

　倒れたマンティコアを見下ろし、肩を上下に激しく揺らす。

　これで決着がついたのか……。

　起きないでくれという心の願いも虚しく――

@move f=マンティコア@屈辱 x=0 y=0 w=100 h=100 opa=255 t=500 wt=1

@se f=締め付ける
@move f=マンティコア@屈辱 x=10 y=0 w=100 h=100 opa=255 t=50 wt=1
@move f=マンティコア@屈辱 x=-10 y=0 w=100 h=100 opa=255 t=50 wt=1
@move f=マンティコア@屈辱 x=10 y=0 w=100 h=100 opa=255 t=50 wt=1
@move f=マンティコア@屈辱 x=0 y=0 w=100 h=100 opa=255 t=50 wt=1

[マンティコア/mtc0122]「ふぅっ……ふぅっ……ふぅぅっ」

　マンティコアが息を荒げながら起き上がる。

;;★白フラ
@bs f=マンティコア@睨み body=-1 face=0
[マンティコア/mtc0123]「ここまで粘るとはな……だが、これで最後だ……っ！　死ねっ！」

;;★白フラ　ｑｋ
@qk
@flash col=255,255,255,255 t=20 wt=0
@bs f=マンティコア@蔑み_z body=-1 face=0

[リック]「がぁあああっ！」

@se f=潰れる
@flash col=255,0,0,255 t=1000 wt=1

@flash col=255,0,0,255 t=300 wt=1
@flash col=255,0,0,255 t=300 wt=1

@qk t=1000

@se f=潰れる
@flash col=255,0,0,255 t=1000 wt=1

　激痛が全身を駆け巡り、片腕が潰される。

;;★SE　攻撃

　だが――

@se f=風切り音2 pitch=60
@wait t=200

@se f=Blow4
@flash t=300 wt=01

　片腕を落としながらも、マンティコアへ痛恨の一撃を入れることができた。

;;★白フラ
@se f=締め付ける
@move_f f=マンティコア@屈辱 body=-1 face=0 in=c t=100
@move f=マンティコア@屈辱 x=10 y=0 w=100 h=100 opa=255 t=50 wt=1
@move f=マンティコア@屈辱 x=-10 y=0 w=100 h=100 opa=255 t=50 wt=1
@move f=マンティコア@屈辱 x=10 y=0 w=100 h=100 opa=255 t=50 wt=1
@move f=マンティコア@屈辱 x=0 y=0 w=100 h=100 opa=255 t=50 wt=1

@bs f=マンティコア@焦り body=-1 face=0
[マンティコア/mtc0124]「馬鹿な……っ！」

[リック]「お前の期待に応えられなくて悪かったな」

　相打ちになった俺を見たマンティコアが、小さく驚きの声を漏らす。

[リック]（死ぬのは怖い……怖いけど……何度でも生き返るなら、何度死んだとしても倒して見せる！）

[リック]「生き返る度に、お前が不利になるぞマンティコア」

;;★白フラ　ｑｋ
;;★ZOOM

@bs f=マンティコア@蔑み_z body=-1 face=0 x=c

[マンティコア/mtc0125]「ほざけぇええっ！」

;;★白フラ　ｑｋ

[リック]「はぁあああああっ！」

@se f=風切り音2
@bs f=マンティコア@蔑み_z body=-1 face=0

@se f=締め付ける
@move f=マンティコア@蔑み x=10 y=0 w=100 h=100 opa=255 t=50 wt=1
@move f=マンティコア@蔑み x=-10 y=0 w=100 h=100 opa=255 t=50 wt=1
@move f=マンティコア@蔑み x=10 y=0 w=100 h=100 opa=255 t=50 wt=1
@move f=マンティコア@蔑み x=0 y=0 w=100 h=100 opa=255 t=50 wt=1

@se f=風切り音1
@flash t=1000 wt=0
@move_f f=マンティコア@ face=0 body=-1 out=c t=20 wt=0

@wait t=250

@se f=風切り音2
@move_f f=effect/effect5@02 face=0 in=c t=50 wt=1

@se f=Sword2
@move_f f=effect/打撃@03_1 face=0 in=c t=50 wt=1

@flash t=300 wt=0
@move_f f=effect/effect2@13 face=0 in=c t=100 wt=1
@move_f f=effect/effect5@ face=0 out=c t=20 wt=1
@qk

@move_f f=effect/effect2@ face=0 out=c t=20 wt=1
@move_f f=effect/打撃@02_2 face=0 in=c t=20 wt=1

@se f=Sword2
@move_f f=effect/effect2@22 face=0 in=c t=50 wt=1
@qk

@se f=Blow4
@move_f f=effect/打撃@04 face=0 in=c t=250 wt=1
@qk t=500

@flash t=300 wt=0
@move_f f=effect/打撃@ face=0 out=c t=20 wt=0

@se f=風切り音1
@move_f f=effect/引っ叩く@04_2 face=0 in=c t=50 wt=1

@se f=風切り音2
@move_f f=effect/引っ叩く01@02 face=0 in=c t=20 wt=1

@move_f f=effect/打撃@02_2 face=0 in=c t=20 wt=1
@se f=Sword1
@move_f f=effect/effect2@ face=0 out=c t=20 wt=0
@move_f f=effect/打撃@03_3 face=0 in=c t=50 wt=1
@flash t=300 wt=0
@qk

@se f=Blow4
@flash t=500 wt=0
@fade type=out t=500 wt=1
@move_f f=effect/打撃@ face=0 out=c t=20 wt=0
@move_f f=effect/引っ叩く01@ face=0 out=c t=20 wt=0
@move_f f=effect/引っ叩く@ face=0 out=c t=20 wt=1

;;背景■暗転

;;効果■時間経過
@move_f f=マンティコア@ face=0 body=-1 out=c t=20 wt=0

@bs
@se
@bgm
@bgs
@bgv
@rps

@ev f=砂漠遺跡
@bs f=マンティコア@屈辱 body=-1 face=0 t=20 wt=0

@move f=砂漠遺跡 x=0 y=0 w=120 h=120 opa=255 t=20 wt=1

@fade type=in t=1000 wt=1

[マンティコア/mtc0126]「ぐっ……うぐぅぅっ……」

[リック]「まだ……全力じゃないんだろう？　最後まで付き合ってやる」

　そう簡単に終わるとは思っていない。

　打倒したマンティコアが起き上がるのを、剣を構えたまま待つ。

@bs f=マンティコア@焦り body=-1 face=0

;;★白フラ
@se f=締め付ける
@move f=マンティコア@蔑み x=10 y=0 w=100 h=100 opa=255 t=50 wt=1
@move f=マンティコア@蔑み x=-10 y=0 w=100 h=100 opa=255 t=50 wt=1
@move f=マンティコア@蔑み x=10 y=0 w=100 h=100 opa=255 t=50 wt=1
@move f=マンティコア@蔑み x=0 y=0 w=100 h=100 opa=255 t=50 wt=1

[マンティコア/mtc0127]「馬鹿な……馬鹿な……馬鹿な……っ！　何故だっ！」

@bs f=マンティコア@睨み face=0 body=-1
[マンティコア/mtc0128]「精剣のエネルギー量は魂に由来している……」

;;★ZOOM
@bs f=マンティコア@屈辱 body=-1 face=0 x=c
[マンティコア/mtc0129]「なのに……明らかにお前の力は人間の魂の総量を超えている！」

@bs f=マンティコア@焦り face=0 body=-1
[マンティコア/mtc0130]「そもそも魔物しか扱えないはずの精魂を使用している時点でおかしいと思ったが、まさかお前は……いや、しかしそんなはずはない！」

　ユラリと起き上がったマンティコアの声に浮かぶ焦りの色。

[リック]（威圧感もそれ程感じなくなっている……）

　既に全力に近い力をマンティコアは引き出しているのかもしれない。

[リック]（勝てるかも……もう少しでマンティコアを倒す事が出来るかもしれない……）

　マンティコアの焦りを感じ取ると、剣を握る手に力がこもる。

@bs f=マンティコア@屈辱 body=-1 face=0

@se f=締め付ける
@move f=マンティコア@屈辱 x=10 y=0 w=100 h=100 opa=255 t=50 wt=1
@move f=マンティコア@屈辱 x=-10 y=0 w=100 h=100 opa=255 t=50 wt=1
@move f=マンティコア@屈辱 x=10 y=0 w=100 h=100 opa=255 t=50 wt=1
@move f=マンティコア@屈辱 x=0 y=0 w=100 h=100 opa=255 t=50 wt=1

[マンティコア/mtc0131]「ふざけるな……っ！　こんな事があって良い筈が無い！　ォおおおおおおおっ！」

;;★白フラ　ｑｋ

[リック]「来い……っ！」

　ガムシャラに突っ込んでくるマンティコアを迎え撃つ俺は、不思議と気持ちは落ちついていた。

@bs f=マンティコア@焦り body=-1 face=0
[マンティコア/mtc0132]「ぐっ……がぁぁ……そんな……」

;;★SE　倒れる

@move f=マンティコア@焦り x=0 y=384 w=100 h=100 opa=0 t=200 wt=1

@se f=床にドサッ
@qk

　マンティコアが土煙を立てて倒れ込む。

[リック]「はっ、はっ、はっ……お前を倒すまで……何度でも相手になる……」

;;★ｑｋ

@move f=マンティコア@焦り x=0 y=0 w=100 h=100 opa=255 t=500 wt=1
@bs f=マンティコア@屈辱 body=-1 face=0

@move f=マンティコア@屈辱 x=10 y=0 w=100 h=100 opa=255 t=50 wt=1
@move f=マンティコア@屈辱 x=-10 y=0 w=100 h=100 opa=255 t=50 wt=1
@move f=マンティコア@屈辱 x=10 y=0 w=100 h=100 opa=255 t=50 wt=1
@move f=マンティコア@屈辱 x=0 y=0 w=100 h=100 opa=255 t=50 wt=1

[マンティコア/mtc0133]「まだっ……まだ、終わらん……終わって……たまるか……っ……私は……精魂をっ……」

@move_f f=マンティコア@屈辱 body=-1 face=0 out=c t=300 wt=1

　マンティコアの力への執念に背筋が震える。

[リック]（……これだけ強いのに、どうしてまだ力を欲しがっている？）

　今までのモンスター娘とは比べ物にならない程の力を持っていながら、更なる力を欲した。

　マンティコアの気持ちが分からずにいると――

;;★BGM停止
@bgm

@mes_win back=1 pos=1
[ジ・オルテ/olt0001]「情けない」
@mes_win back=0 pos=2
;;ここからしばらくジ・オルテの名前が出てきますが、？？？の方が良いのではないでしょうか。

　不意に声が響いてくる。

;;★白フラ
@flash col=255,255,255,255 t=300
[リック]「だ、誰だ……っ！？」

　突然の声に慌てて周囲を見回すと――

;;★白フラ　ｑｋ
@qk
@flash col=255,255,255,255 t=300
[リック]「っっっっ！」

;;★BGM　ボス登場
@bgm f=Battle6

　まがまがしく凶悪な気配が辺りに満ち始める。

;;★白フラ　ｑｋ
@qk
@flash col=255,255,255,255 t=300
[リック]「な、何だ……くっ……ぅぅぅっ」

　その圧倒的な力の気配を感じ取ると、体の震えが止まらなくなってしまう。

@mes_win back=1 pos=1
[ジ・オルテ/olt0002]「人間ごときに負けてしまうとは」
@mes_win back=0 pos=2


　冷たい瞳が、マンティコアへと注がれる。

;;★ｑｋ

　マンティコアは、その突然現れたモンスター娘に驚いた目を向けるも、すぐに怯えたように顔を俯かせる。

@bs
@bg f=砂漠遺跡
@fade type=in t=500 wt=1

@mes_win back=1 pos=1
[ジ・オルテ/olt0003]「しかし……マンティコアに勝利したお前は……賞賛するに値するわ」

[ジ・オルテ/olt0004]「褒美として、私達の世界に招待してあげましょう」
@mes_win back=0 pos=2

[リック]「え……？」

;;★こんな感じの演出
@se f=Growl pitch=90
@move_f f=effect/エフェクト03@02 face=0 in=c opa=100 t=300 wt=1
@move_f f=effect/エフェクト03@01 face=0 in=c opa=100 t=300 wt=1
@move_f f=effect/エフェクト03@02 face=0 in=c opa=100 t=300 wt=1
@wait t=300
@bs

@bg f=砂漠遺跡
@fade type=in t=500 wt=1

　反論する暇もなく、グラリと足元が崩れ去るような感覚を覚えると――

　瞬く間に視界がグニャグニャと歪み出す。

;;★白フラ　ｑｋ
@qk
@flash col=255,255,255,255 t=300
[リック]「うわっ！？　うわあああっ！」

　自分の悲鳴も、どこか他人のような声に聞きながら、深く深く闇の中へと落ちていった。

;;背景■暗転

;;効果■時間経過

@fade tyoe=out t=250 wt=1

@bs
@se
@bgm
@bgs
@bgv
@rps

@fade tyoe=in t=20 wt=1

;;============================
