;;============================

;;★
;;背景■暗転

;;★
;;効果■時間経過
@fade type=out t=1000 wt=1
@wait t=1000
@ev f=森
@move f=森 x=0 y=0 w=120 h=120 opa=255 t=20 wt=1

;;★BGM　シリアス
@bgm f=003

@fade type=in t=1000 wt=1

[リック]（うっ……うぅっ……くそっ！　もう一度だ！）

;;★BGM　ピンチ
@bgm f=004

@fade type=out t=200 wt=1
@bs
@bg f=森
@bs f=ゴブリン娘@怒り body=-1 face=0 x=c
@fade type=in t=200 wt=1

;;★EF　白フラ　ｑｋ
@qk
@flash col=255,255,255,255 t=300
[リック]「うわぁあああああああああっ！」

;;============================
