;;============================

;;★BGM　ピンチ

@wait t=2000
;;効果■時間経過

@fade type=out t=20 wt=1

@se f=Flash1
@bs f=effect/魔法1@溜め_1 face=0 body=-1
@bs f=A_white face=0 body=-1 t=20 wt=1
@move f=effect/魔法1@溜め_1 x=0 y=0 w=120 h=120 opa=150 t=20 wt=1
@fade type=in t=100 wt=1

@fade type=out t=200 wt=1
@fade type=in t=200 wt=1

@fade type=out t=200 wt=1
@fade type=in t=200 wt=1

@se f=Flash3
@move_f f=A_white face=0 out=c t=2000 wt=1

;;★SE　水ごぼごぼ
@se f=チャプチャプという水の音
@wait t=100

@se f=「ドクンッ」という心音
@qk

@se f=チャプチャプという水の音
@qk t=1000

[リック]「うっ……ぐぶっ！？　がぼぉっ！？」

　水底で意識が戻ると、水泡が水面に向かって浮かび上がっていく。

;;★白フラ　ｑｋ
@se f=チャプチャプという水の音
@qk t=1000

[リック]（く、苦しい……このままじゃ……また死んじゃう……っ！）

;;★SE　水ごぼごぼ
@se f=チャプチャプという水の音
@qk t=1000

@se f=Water1

@fade type=out t=100 wt=0
@move f=effect/魔法1@溜め_1 x=0 y=76 w=120 h=120 opa=150 t=100 wt=1

　薄暗い水底から、光の方へと向かって必死に手足を動かしていく。

@bs wt=0
@ev f=湖
@move f=湖 x=0 y=0 w=120 h=120 opa=255 t=20 wt=1

@bgm f=005 pitch=60

;;背景■湖周辺

;;★SE　水ばしゃ
@se f=Water1
@fade type=in t=500 wt=0
@qk t=1000

;;★白フラ　ｑｋ

[リック]「ぷはぁあっ！　はっ！　はっ！　はぁっ！」

　岸へと這い上がると、四つん這いになったまま荒々しく息を吐く。

;;★動かす

@se f=Water1
@bgm

@bs f=ドクターフィッシュ@睨み body=-1 face=0 opa=0 y=100
@move f=ドクターフィッシュ@睨み x=0 y=50 w=100 h=100 opa=255 t=150 wt=1
@move f=ドクターフィッシュ@睨み x=0 y=0 w=100 h=100 opa=255 t=200 wt=1
@move f=ドクターフィッシュ@睨み x=0 y=50 w=100 h=100 opa=255 t=200 wt=1

[ドクターフィッシュ１/dca0017]「エサ？　逃げた？」

@move_f f=ドクターフィッシュ@ face=0 out=c t=250

;;★SE　水ばしゃばしゃ
@se f=Water1
@wait t=50

@bgm f=004

@se f=Water1
@wait t=100

@bs f=ドクターフィッシュ@普通 body=-1 face=0 opa=0 y=100 x=250
@move f=ドクターフィッシュ@普通 x=250 y=50 w=100 h=100 opa=255 t=150 wt=1
@move f=ドクターフィッシュ@普通 x=250 y=0 w=100 h=100 opa=255 t=200 wt=1
@move f=ドクターフィッシュ@普通 x=250 y=50 w=100 h=100 opa=255 t=200 wt=1

@wait t=250
@move_f f=ドクターフィッシュ@ face=0 out=c t=250

;;★SE　水ばしゃばしゃ
@se f=Water1
@wait t=50
@se f=Water1
@wait t=100

@bs f=ドクターフィッシュ@睨み body=-1 face=0 opa=0 y=100 x=-100
@move f=ドクターフィッシュ@睨み x=-100 y=50 w=100 h=100 opa=255 t=150 wt=1
@move f=ドクターフィッシュ@睨み x=-100 y=0 w=100 h=100 opa=255 t=200 wt=1
@move f=ドクターフィッシュ@睨み x=-100 y=50 w=100 h=100 opa=255 t=200 wt=1

@wait t=250
@move_f f=ドクターフィッシュ@ face=0 out=c t=250

　水飛沫の音と共に、ドクターフィッシュ娘が飛び出てくる。

@bs f=ドクターフィッシュ@笑み body=-1 face=0 y=50

@move f=ドクターフィッシュ@笑み x=-0 y=0 w=100 h=100 t=100 wt=1
@move f=ドクターフィッシュ@笑み x=-0 y=50 w=100 h=100 t=80 wt=1
@move f=ドクターフィッシュ@笑み x=-0 y=0 w=100 h=100 t=100 wt=1
@move f=ドクターフィッシュ@笑み x=-0 y=50 w=100 h=100 t=80 wt=1

[リック]（も、もう騙されないぞ！）


;;============================
