;;============================
;;背景■森
;;★BGM　モンスター
@bgm

@fade type=out t=20 wt=1

@ev f=森
@bs f=蜘蛛娘@普通 body=-1 face=0
@move f=森 x=0 y=0 w=120 h=120 opa=255 t=20 wt=1

@fade type=in t=1000 wt=1

@se f=「ドクンッ」という心音
@flash t=500 wt=0
@qk t=500 wt=1

@bs f=蜘蛛娘@焦り body=-1 face=0 t=20 wt=1

[アラクネ/arc0067]「ぐっ……？　うぇっ！？」

@se f=「ドクンッ」という心音
@flash t=500 wt=0
@qk t=500 wt=1

@se f=締め付ける
@flash t=300 wt=0
@bs f=蜘蛛娘@ body=-1 face=0 t=20 wt=1
@qk

;;★SE　液体吐く
@se f=産まれる
@flash t=500 wt=1

　アラクネが僅かに吐き出したどろどろの液体。

@se f=Flash3
@flash t=1000 wt=1

　消化液によって溶かされてしまった肉片が、瞬く間に俺の体を形作っていく。

@bs f=蜘蛛娘@睨み body=-1 face=0 opa=0
@move f=蜘蛛娘@睨み x=0 y=0 w=100 h=100 opa=255 t=250 wt=1

[アラクネ/arc0068]「あなた……人間なの……？」

　アラクネの顔に浮かぶ嫌悪感。

　モンスター娘に、そんな風に見られる俺って……。



;;============================
