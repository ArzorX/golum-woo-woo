;;============================
;;背景■街道周辺
;;★BGM　ピンチ
@bgm

@ev f=森

@se f=歩く_遠
@fade type=in t=1600 wt=0

@move f=森 x=0 y=19  w=105 h=105 opa=255 t=400 wt=1
@move f=森 x=0 y=-19 w=110 h=110 opa=255 t=400 wt=1

@move f=森 x=0 y=19  w=115 h=115 opa=255 t=400 wt=1
@move f=森 x=0 y=-19 w=120 h=120 opa=255 t=400 wt=1

@move f=森 x=0 y=19  w=125 h=125 opa=255 t=400 wt=1
@move f=森 x=0 y=0 w=130 h=130 opa=255 t=400 wt=1



;;★赤フラ　ｑｋ
@flash col=255,255,255,255 t=500 wt=0
@qk t=500

[女]「きゃぁああああっ！」

;;　突如、悲鳴が聞こえてきた。

;;★白フラ
@se f=「ドクンッ」という心音
@flash col=255,255,255,255 t=300 wt=0
@qk

@bgm f=003

[リック]「っっ！！？」

;;★SE　走る
;;★時間経過

@se f=歩く_遠
@fade type=out t=1200 wt=0

@move f=森 x=0 y=19  w=135 h=135 opa=255 t=200 wt=1
@move f=森 x=0 y=-19 w=140 h=140 opa=255 t=200 wt=1

@move f=森 x=0 y=19  w=145 h=145 opa=255 t=200 wt=1
@move f=森 x=0 y=0   w=150 h=150 opa=255 t=200 wt=1

@move f=森 x=0 y=19  w=155 h=155 opa=255 t=200 wt=1
@move f=森 x=0 y=-19 w=160 h=160 opa=255 t=200 wt=1

@bs
@wait t=2700

@ev f=街道

@bgm
@se f=走る_遠
@fade type=in t=1600 wt=0

@move f=街道 x=0 y=19  w=105 h=105 opa=255 t=200 wt=1
@move f=街道 x=0 y=-19 w=110 h=110 opa=255 t=200 wt=1

@move f=街道 x=0 y=19  w=115 h=115 opa=255 t=200 wt=1
@move f=街道 x=0 y=-19 w=120 h=120 opa=255 t=200 wt=1

@move f=街道 x=0 y=19  w=125 h=125 opa=255 t=200 wt=1
@move f=街道 x=0 y=0 w=130 h=130 opa=255 t=200 wt=1

;;　思わず悲鳴のした方へと駆け出し街道に出ると――

@bgm f=004

@bs f=娘 face=0 body=-1 x=400 y=50 opa=0 t=20 wt=0
@bs f=母親 face=0 body=-1 x=250 y=50 opa=0 t=20 wt=0
@bs f=モンスターＢ face=0 body=-1 x=-300 y=50 opa=0 t=20 wt=0

@move f=娘 x=400 y=50 w=100 h=100 opa=255 t=250 wt=0
@move f=母親 x=250 y=50 w=100 h=100 opa=255 t=250 wt=0
@move f=モンスターＢ x=-300 y=50 w=100 h=100 opa=255 t=250 wt=1

@move f=娘 x=410 y=50 w=100 h=100 opa=255 t=100 wt=1
@move f=娘 x=390 y=50 w=100 h=100 opa=255 t=100 wt=1
@move f=娘 x=410 y=50 w=100 h=100 opa=255 t=100 wt=1
@move f=娘 x=400 y=50 w=100 h=100 opa=255 t=100 wt=1

[娘]「ママ、怖いよぉ……！」

@move f=母親 x=250 y=0 w=100 h=100 t=150 wt=1
@move f=母親 x=250 y=50 w=100 h=100 t=120 wt=1

[母親]「だ、大丈夫よ……ママが……ママが一緒にいるから」

　モンスター娘を前にして、へたり込んでいる母娘の姿が視界に入ってきた。

[リック]（うぅ、思わず来ちゃったけど……）

[モンスター娘]「ちっ、雌か。食い物にしかならないなぁ。雄だったら良かったのに」

@move f=モンスターＢ x=-300 y=0 w=100 h=100 t=150 wt=1
@move f=モンスターＢ x=-300 y=50 w=100 h=100 t=120 wt=1

[モンスター娘]「まぁ、良いか。あんまりお腹空いてないけど食べちゃうか」

@move f=娘 x=410 y=50 w=100 h=100 opa=255 t=100 wt=1
@move f=娘 x=390 y=50 w=100 h=100 opa=255 t=100 wt=1
@move f=娘 x=410 y=50 w=100 h=100 opa=255 t=100 wt=1
@move f=娘 x=400 y=50 w=100 h=100 opa=255 t=100 wt=1

[娘]「ママ～～」

@move f=母親 x=260 y=50 w=100 h=100 opa=255 t=100 wt=1
@move f=母親 x=240 y=50 w=100 h=100 opa=255 t=100 wt=1
@move f=母親 x=260 y=50 w=100 h=100 opa=255 t=100 wt=1
@move f=母親 x=250 y=50 w=100 h=100 opa=255 t=100 wt=1

[母親]「この子は……この子だけでも助けて下さい！」

@move f=モンスターＢ x=-300 y=0 w=100 h=100 t=100 wt=1
@move f=モンスターＢ x=-300 y=50 w=100 h=100 t=70 wt=1
@move f=モンスターＢ x=-300 y=0 w=100 h=100 t=100 wt=1
@move f=モンスターＢ x=-300 y=50 w=100 h=100 t=70 wt=1

[モンスター娘]「ふんっ、子供の方が肉は柔らかくて美味しいからね♪　まっ、親子仲良く食べてあげるから♪」

@move f=蜘蛛娘@微笑 x=256 y=50 w=100 h=100 opa=255 t=100 wt=0

@move f=娘 x=48 y=50 w=100 h=100 opa=255 t=100 wt=0
@move f=母親 x=-12 y=50 w=100 h=100 opa=255 t=100 wt=0
@move f=モンスターＢ x=-562 y=50 w=100 h=100 opa=255 t=100 wt=0
@move f=街道 x=-256 y=0 w=130 h=130 opa=255 t=100 wt=0
@fade type=out t=100 wt=1

@bs

@ev f=森
@move f=森 x=0 y=0 w=120 h=120 opa=255 t=20 wt=1

@fade type=in t=500 wt=1

@se f=Sword4
@move_f f=effect/剣 face=0 in=c t=300 wt=1

[リック]（し、仕方ない……！　お、俺がやるしかない！）

@move_f f=effect/剣 face=0 out=c t=300 wt=1

@bgm

@move f=森 x=256 y=0 w=130 h=130 opa=255 t=100 wt=0
@fade type=out t=100 wt=1
@bs

@ev f=街道
@bs f=effect/集中線@01 face=0 body=1

@se f=風切り音2
@move f=effect/集中線@01 x=0 y=0 w=120 h=120 opa=70 t=250 wt=0
@move f=街道 x=0 y=0 w=130 h=130 opa=255 t=250 wt=0
@fade type=in t=200 wt=1

;;　か弱い母娘を見捨てて逃げるなんて出来ない。
;;　幸いにして、何度かモンスター娘を倒してきている。
;;　今、目の前にいるモンスター娘は、今まで倒してきたのよりは弱そうに見えた。

@bgm f=006

;;★ｑｋ
@bs f=モンスターＢ face=0 body=-1 y=50
@qk t=500

@move f=effect/集中線@01 x=0 y=0 w=120 h=120 opa=0 t=100 wt=0

[リック]「止めろぉおおおっ！」

@move f=モンスターＢ x=0 y=0 w=100 h=100 t=150 wt=1
@move f=モンスターＢ x=0 y=50 w=100 h=100 t=120 wt=1

[モンスター娘]「え！？　うぁっ！？」


@se f=風切り音2 pitch=60
@move_f f=effect/斬る@00 face=0 in=c t=100 wt=1
@move_f f=effect/斬る@00 face=0 out=c t=50 wt=1

@se f=風切り音2 pitch=60
@move_f f=effect/effect3@02 face=0 in=c t=100 wt=1
@move_f f=effect/effect3@02 face=0 out=c t=50 wt=1

@se f=壁に「ドンッ」
@move f=モンスターＢ x=10 y=50 w=100 h=100 opa=255 t=50 wt=1
@move f=モンスターＢ x=-10 y=50 w=100 h=100 opa=255 t=50 wt=1
@move f=モンスターＢ x=10 y=50 w=100 h=100 opa=255 t=50 wt=1
@move f=モンスターＢ x=0 y=50 w=100 h=100 opa=255 t=50 wt=1

@flash col=255,255,255,255 t=300

[リック]「はぁあああああっ！」


;;============================
