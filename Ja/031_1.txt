;;============================
;;★BGM　敗北

@fade type=in t=20 wt=1

@se f=Flash1
@bs f=A_white face=0 body=-1 t=20 wt=1
@fade type=in t=100 wt=1

@fade type=out t=200 wt=1
@fade type=in t=200 wt=1

@fade type=out t=200 wt=1
@fade type=in t=200 wt=1

@se f=Flash3
@move_f f=A_white face=0 out=c t=2000 wt=1

@wait t=1000
@fade type=out t=20 wt=1

@bs f=空 face=0 body=-1 opa=0
@bs f=white face=0 body=-1
@bs f=eyelid_u face=0 body=-1
@bs f=eyelid_b face=0 body=-1

@move f=空 x=0 y=76 w=120 h=120 opa=255 t=20 wt=0
@move f=white x=0 y=0 w=120 h=120 opa=255 t=20 wt=1

@fade type=in t=300 wt=0
@move f=eyelid_u x=0 y=-384 h=100 w=100 opa=0 t=1500 wt=0
@move f=eyelid_b x=0 y=384 h=100 w=100 opa=0 t=1500 wt=0
@move f=white x=0 y=0 h=100 w=100 opa=0 t=1500 wt=1

[リック]（っ……ここは……っ！）

;;★BGM　モンスター
@bgm f=003

@se f=衣擦れの音1
@move f=空 x=0 y=-70 w=120 h=120 opa=255 t=100 wt=0
@fade type=out t=100 wt=1
@bs wt=0

@ev f=街道
@move f=街道 x=0 y=-70 w=120 h=120 opa=255 t=20 wt=1

@fade type=in t=100 wt=1

[リック]（く、くそっ！　もう一度だ！）

[リック]「……う、うわぁああっ！」

@bgm f=004

　チャンスを逃せば、またさっきと同じように死ぬまで搾られるだけ。

;;★ZOOM
@se f=走る_コンクリート
@bs f=effect/集中線@01 face=0 body=-1 opa=70

@se f=風切り音2
@move f=effect/集中線@01 x=0 y=0 w=100 h=100 opa=0 t=300 wt=0
@move_f f=リザードマン@屈辱_z face=0 in=c t=500

;;★白フラ　ｑｋ
;;★SE　走る

[リック]「あぁああああああっ！」

@se f=締め付ける
@qk

　手にした剣を力いっぱい握りしめ、リザードマンに向かって切りかかる。


;;============================
