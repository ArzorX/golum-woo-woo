;;============================
;;背景■どこか分からない暗闇のような場所
;;※第三者視点になっています。

@fade type=out t=20 wt=1

@wait t=2000

;;★BGM　モンスター城
@bgm f=003

@ev f=要塞内部
@move f=要塞内部 x=0 y=-76 w=130 h=130 opa=255 t=20 wt=1 
@fade type=in t=1000 wt=1

　光の届かない闇の奥深い場所。

　そこに四体のモンスター娘達が集まっていた。

@move_f f=ドッペルゲンガー@元 face=0 in=c t=250
@wait t=250

@move f=ドッペルゲンガー@元 x=-256 y=0 w=100 h=100 opa=255 t=100 wt=0
@move f=要塞内部 x=-256 y=-76 w=130 h=130 opa=255 t=100 wt=0
@fade type=out t=100 wt=1

@move_f f=ドッペルゲンガー@ face=0 out=c t=20 wt=0
@bs wt=0

@ev f=要塞内部
@move f=要塞内部 x=0 y=-76 w=130 h=130 opa=255 t=20 wt=1 

@bs f=蒸気娘@普通 face=0 body=-1

@fade type=in t=500 wt=1
@wait t=250

@move f=蒸気娘@普通 x=-256 y=0 w=100 h=100 opa=255 t=100 wt=0
@move f=要塞内部 x=-256 y=-76 w=130 h=130 opa=255 t=100 wt=0
@fade type=out t=100 wt=1

@move_f f=蒸気娘@ face=0 out=c t=20 wt=0
@bs wt=0

@ev f=要塞内部
@move f=要塞内部 x=0 y=-76 w=130 h=130 opa=255 t=20 wt=1 

@bs f=淫蚊娘@普通 face=0 body=-1

@fade type=in t=500 wt=1
@wait t=250

@move f=淫蚊娘@普通 x=-256 y=0 w=100 h=100 opa=255 t=100 wt=0
@move f=要塞内部 x=-256 y=-76 w=130 h=130 opa=255 t=100 wt=0
@fade type=out t=100 wt=1

@move_f f=淫蚊娘@ face=0 out=c t=20 wt=0
@bs wt=0

@ev f=要塞内部
@move f=要塞内部 x=0 y=-76 w=130 h=130 opa=255 t=20 wt=1 

@bs f=石化蛇娘@普通 face=0 body=-1

@fade type=in t=500 wt=1
@wait t=250

@move f=石化蛇娘@普通 x=-256 y=0 w=100 h=100 opa=255 t=100 wt=0
@move f=要塞内部 x=-256 y=-76 w=130 h=130 opa=255 t=100 wt=0
@fade type=out t=100 wt=1

@move_f f=石化蛇娘@ face=0 out=c t=20 wt=0
@bs wt=0

@ev f=要塞内部
@move f=要塞内部 x=0 y=-76 w=130 h=130 opa=255 t=20 wt=1 

@fade type=in t=500 wt=1

@bs f=蒸気娘@普通 face=0 body=-1 opa=0 t=20 wt=0
@bs f=ドッペルゲンガー@元 face=0 body=1 opa=0 t=20 wt=0
@bs f=淫蚊娘@普通 face=0 body=-1 opa=0 t=20 wt=0
@bs f=石化蛇娘@普通 face=0 body=-1 opa=0 t=20 wt=0

@move f=ドッペルゲンガー@元 x=300 y=0 w=100 h=100 opa=0 t=20 wt=0
@move f=蒸気娘@普通 x=-300 y=0 w=100 h=100 opa=0 t=20 wt=0
@move f=淫蚊娘@普通 x=150 y=77 w=80 h=80 opa=0 t=20 wt=0
@move f=石化蛇娘@普通 x=-150 y=77 w=80 h=80 opa=0 t=20 wt=1

@move f=ドッペルゲンガー@元 x=300 y=-38 w=110 h=110 opa=255 t=1000 wt=0
@move f=蒸気娘@普通 x=-300 y=-38 w=110 h=110 opa=255 t=1000 wt=0
@move f=淫蚊娘@普通 x=200 y=77 w=80 h=80 opa=255 t=1000 wt=0
@move f=石化蛇娘@普通 x=-200 y=77 w=80 h=80 opa=255 t=1000 wt=1

;;@move f= x=0 y=0 w=100 h=100 opa=255 t=20 wt=0
;;@move f= x=0 y=0 w=100 h=100 opa=255 t=20 wt=0
;;@move f= x=0 y=0 w=100 h=100 opa=255 t=20 wt=0
;;@move f= x=0 y=0 w=100 h=100 opa=255 t=20 wt=1

;;f=蒸気娘@普通
;;f=ドッペルゲンガー@元
;;f=淫蚊娘@普通
;;f=石化蛇娘@普通

　ドッペルゲンガー、蒸気娘、淫蚊娘、ゴルゴーンの四人。

　彼女たちは、モンスター娘のなかでも頭一つ飛びぬけた力を持つ為――

　四天王と呼ばれている存在だった。

@move f=ドッペルゲンガー@元 x=300 y=-38 w=110 h=110 opa=0 t=250 wt=0
@move f=蒸気娘@普通 x=-300 y=-38 w=110 h=110 opa=0 t=250 wt=0
@move f=淫蚊娘@普通 x=200 y=77 w=80 h=80 opa=0 t=250 wt=0
@move f=石化蛇娘@普通 x=-200 y=77 w=80 h=80 opa=0 t=250 wt=1

@move_f f=蒸気娘@普通 face=0 out=c t=20 wt=0
@move_f f=ドッペルゲンガー@元 face=0 out=c t=20 wt=0
@move_f f=淫蚊娘@普通 face=0 out=c t=20 wt=0
@move_f f=石化蛇娘@普通 face=0 out=c t=20 wt=1

@bs f=蒸気娘@普通 body=-1 face=0 y=50 opa=0 t=20 wt=0
@move f=蒸気娘@普通 x=0 y=50 w=100 h=100 opa=255 t=250 wt=1

[アトミス/jok0001]「あの人間……順調に進んでいるようだな」

@move_f f=蒸気娘@ face=0 out=c t=300 wt=1

;;★動かす
@bs f=石化蛇娘@睨み body=-1 face=0 x=300 y=50 opa=0 t=20 wt=0
@move f=石化蛇娘@睨み x=300 y=50 w=100 h=100 opa=255 t=250 wt=1

[ゴルゴーン/glg0001]「ふんっ、そうでなければ困る」

@bs f=淫蚊娘@普通 body=-1 face=0 x=-300 y=50 opa=0 t=20 wt=0
@move f=淫蚊娘@普通 x=-300 y=50 w=100 h=100 opa=255 t=250 wt=1

[モスキーナ/msk0001]「何でも人間たちの間では勇者が現れて、世界を救ってくれるって噂になってるみたいよ」

@move f=淫蚊娘@普通 x=-300 y=50 w=100 h=100 opa=0 t=250 wt=0
@move f=石化蛇娘@睨み x=300 y=50 w=100 h=100 opa=0 t=250 wt=1

@move_f f=淫蚊娘@普通 face=0 out=c t=20 wt=0
@move_f f=石化蛇娘@普通 face=0 out=c t=20 wt=1

@bs f=蒸気娘@拗ねる body=-1 face=0 x=-300 y=50 opa=0 t=20 wt=0
@move f=蒸気娘@拗ねる x=-300 y=50 w=100 h=100 opa=255 t=250 wt=1

[アトミス/jok0002]「……勇者？」

@bs f=ドッペルゲンガー@元 body=-1 face=0 x=300 y=50 opa=0 t=20 wt=0
@move f=ドッペルゲンガー@元 x=300 y=50 w=100 h=100 opa=255 t=250 wt=1

[ドッペルゲンガー/dpg0001]「勇者とは笑わせるね。でも、そう言われるくらいには強いんじゃない？」

@move f=蒸気娘@拗ねる x=-300 y=50 w=100 h=100 opa=0 t=250 wt=0
@move f=ドッペルゲンガー@元 x=300 y=50 w=100 h=100 opa=0 t=250 wt=1

@move_f f=蒸気娘@ face=0 out=c t=20 wt=0
@move_f f=ドッペルゲンガー@ face=0 out=c t=20 wt=0

@bs f=淫蚊娘@笑み body=-1 face=0 x=-300 y=50 opa=0 t=20 wt=0
@move f=淫蚊娘@笑み x=-300 y=50 w=100 h=100 opa=255 t=250 wt=1

@move f=淫蚊娘@笑み x=-300 y=0 w=100 h=100 t=150 wt=1
@move f=淫蚊娘@笑み x=-300 y=50 w=100 h=100 t=120 wt=1

[モスキーナ/msk0002]「へぇ、そうなんだ？　ちょっと興味あるかも♪」

@bs f=石化蛇娘@屈辱 body=-1 face=0 x=300 y=50 opa=0 t=20 wt=0
@move f=石化蛇娘@屈辱 x=300 y=50 w=100 h=100 opa=255 t=250 wt=1

[ゴルゴーン/glg0002]「人間の集団に倒されるのは珍しくもないが……一人で我らの仲間たちを倒していると言うのか？」

@move f=淫蚊娘@笑み x=-300 y=50 w=100 h=100 opa=0 t=250 wt=0
@move f=石化蛇娘@屈辱 x=300 y=50 w=100 h=100 opa=0 t=250 wt=1

@move_f f=淫蚊娘@ face=0 out=c t=20 wt=0
@move_f f=石化蛇娘@ face=0 out=c t=20 wt=1

@bs f=蒸気娘@微笑 body=-1 face=0 x=-300 y=50 opa=0 t=20 wt=0
@move f=蒸気娘@微笑 x=-300 y=50 w=100 h=100 opa=255 t=250 wt=1

[アトミス/jok0003]「倒された仲間たちも雑魚ばかり。一人で倒す事くらいは出来てもおかしくは無い」

@bs f=淫蚊娘@焦り body=-1 face=0 x=300 y=50 opa=0 t=20 wt=0
@move f=淫蚊娘@焦り x=300 y=50 w=100 h=100 opa=255 t=250 wt=1

[モスキーナ/msk0003]「そうよねぇ」

@move f=淫蚊娘@焦り x=300 y=50 w=100 h=100 opa=0 t=250 wt=0
@move f=蒸気娘@微笑 x=-300 y=50 w=100 h=100 opa=0 t=250 wt=1

@move_f f=淫蚊娘@ face=0 out=c t=20 wt=0
@move_f f=蒸気娘@ face=0 out=c t=20 wt=1

@bs f=ドッペルゲンガー@元 body=-1 face=0 x=-300 y=50 opa=0 t=20 wt=0
@move f=ドッペルゲンガー@元 x=-300 y=50 w=100 h=100 opa=255 t=250 wt=1

[ドッペルゲンガー/dpg0002]「ゴルゴーンは心配症ね」

@bs f=石化蛇娘@屈辱 body=-1 face=0 x=300 y=50 opa=0 t=20 wt=0
@move f=石化蛇娘@屈辱 x=300 y=50 w=100 h=100 opa=255 t=250 wt=1

[ゴルゴーン/glg0003]「気をつけるにこした事は無い」

@move f=ドッペルゲンガー@元 x=-300 y=0 w=100 h=100 t=150 wt=1
@move f=ドッペルゲンガー@元 x=-300 y=50 w=100 h=100 t=120 wt=1

[ドッペルゲンガー/dpg0003]「だったら、私の部下に様子を見させてくるわ」

@se f=歩く_遠
@move f=ドッペルゲンガー@元 x=-300 y=50 w=100 h=100 opa=0 t=1000 wt=1

@wait t=250

@move f=石化蛇娘@屈辱 x=44 y=50 w=100 h=100 opa=255 t=100 wt=0
@move f=要塞内部 x=-256 y=-76 w=130 h=130 opa=255 t=100 wt=0
@fade type=out t=100 wt=1

@move_f f=ドッペルゲンガー@ face=0 out=c t=20 wt=0
@move_f f=石化蛇娘@ face=0 out=c t=20 wt=0
@bs wt=0

@ev f=要塞内部
@move f=要塞内部 x=0 y=-76 w=130 h=130 opa=255 t=20 wt=1 

@bs f=ドッペルゲンガー@元 face=0 body=-1

@fade type=in t=500 wt=1

[ドッペルゲンガー/dpg0004]「部下に指示を与えてくるわね」

;;★SE　歩き去る
@se f=歩く_遠
@move_f f=ドッペルゲンガー@ out=c face=0 t=1000

　そう言い残してドッペルゲンガーが部屋を出て行く。
@move f=要塞内部 x=-256 y=-76 w=130 h=130 opa=255 t=100 wt=0
@fade type=out t=100 wt=1

@move_f f=ドッペルゲンガー@ face=0 out=c t=20 wt=0
@move_f f=石化蛇娘@ face=0 out=c t=20 wt=0
@bs wt=0

@ev f=要塞内部
@move f=要塞内部 x=0 y=-76 w=130 h=130 opa=255 t=20 wt=1 

@bs f=石化蛇娘@睨み body=-1 face=0 y=50 t=20 wt=1
@fade type=in t=500 wt=1

[ゴルゴーン/glg0004]「勇者、か……だとしたら、我々のしたことは……」

@move_f f=石化蛇娘@ face=0 out=c t=250 wt=1

@bs f=蒸気娘@笑み body=-1 face=0 x=-300 y=50 opa=0 t=20 wt=0
@move f=蒸気娘@笑み x=-300 y=50 w=100 h=100 opa=255 t=250 wt=1

[アトミス/jok0004]「気にし過ぎ。人間である事に変わりはない」

;;★動かす
@bs f=淫蚊娘@笑み body=-1 face=0 x=300 y=50 opa=0 t=20 wt=0
@move f=淫蚊娘@笑み x=300 y=50 w=100 h=100 opa=255 t=250 wt=1

@move f=淫蚊娘@笑み x=300 y=0 w=100 h=100 t=150 wt=1
@move f=淫蚊娘@笑み x=300 y=50 w=100 h=100 t=120 wt=1

[モスキーナ/msk0004]「そうそう。それに、ちょっと強いくらいじゃ、私達の相手じゃないだろうしね♪」

@move f=蒸気娘@笑み x=-300 y=50 w=100 h=100 opa=0 t=250 wt=0
@move f=淫蚊娘@笑み x=300 y=50 w=100 h=100 opa=0 t=250 wt=1

@move_f f=蒸気娘@ face=0 out=c t=20 wt=0
@move_f f=淫蚊娘@ face=0 out=c t=20 wt=1

@move_f f=石化蛇娘@屈辱 in=c face=0 t=250 wt=1
[ゴルゴーン/glg0005]「……」

@fade type=out t=1000 wt=1

@wait t=1200
@se f=Earth4

@move_f f=石化蛇娘@ out=c face=0 t=20 wt=0
@bs wt=0

@ev f=要塞内部
@move f=要塞内部 x=0 y=-76 w=130 h=130 opa=255 t=20 wt=1 

@fade type=in t=1000 wt=1

;;★SE　歩く近づく
@se f=歩く_遠
@bs f=モンスターＢ face=0 body=-1 opa=0 y=50 t=20 wt=0
@move f=モンスターＢ x=0 y=50 w=100 h=100 opa=255 t=1000 wt=1

@move f=モンスターＢ x=0 y=0 w=100 h=100 t=150 wt=1
@move f=モンスターＢ x=0 y=50 w=100 h=100 t=120 wt=1

[ドッペルゲンガー部下/dpg0444]「失礼します」

　残された四天王たちが話をしていると、モンスター娘の一人が部屋に入ってくる。

@move f=モンスターＢ x=0 y=0 w=100 h=100 t=150 wt=1
@move f=モンスターＢ x=0 y=50 w=100 h=100 t=120 wt=1

[ドッペルゲンガー部下/dpg0445]「ドッペルゲンガー様の命により、これより勇者の調査に行ってまいります」

@move f=モンスターＢ x=-256 y=50 w=100 h=100 opa=255 t=100 wt=0
@move f=要塞内部 x=-256 y=-76 w=130 h=130 opa=255 t=100 wt=0
@fade type=out t=100 wt=1

@move_f f=モンスターＢ face=0 out=c t=20 wt=0
@bs wt=0

@ev f=要塞内部
@move f=要塞内部 x=0 y=-76 w=130 h=130 opa=255 t=20 wt=1 

@move_f f=石化蛇娘@屈辱 face=0 in=c t=20 wt=1
@move f=石化蛇娘@屈辱 x=-300 y=50 w=100 h=100 opa=255 wt=1

@bs f=淫蚊娘@笑み body=-1 face=0 x=300 y=50 t=20 wt=0

@fade type=in t=500 wt=1

@bs f=石化蛇娘@睨み x=-300 y=50 body=-1 face=0
[ゴルゴーン/glg0006]「うむ……よろしく頼む」

[モスキーナ/msk0005]「頑張ってね～♪」

@move f=石化蛇娘@睨み x=-656 y=50 w=100 h=100 opa=255 t=100 wt=0
@move f=淫蚊娘@笑み x=44 y=50 w=100 h=100 opa=255 t=100 wt=0

@move f=要塞内部 x=-256 y=-76 w=130 h=130 opa=255 t=100 wt=0
@fade type=out t=100 wt=1

@move_f f=蒸気娘@普通 face=0 out=c t=20 wt=0
@move_f f=ドッペルゲンガー@元 face=0 out=c t=20 wt=0
@move_f f=淫蚊娘@普通 face=0 out=c t=20 wt=0
@move_f f=石化蛇娘@普通 face=0 out=c t=20 wt=1

@bs wt=0

@ev f=要塞内部
@move f=要塞内部 x=0 y=-76 w=130 h=130 opa=255 t=20 wt=1 

@bs f=モンスターＢ body=-1 y=50 face=0 t=20 wt=0

@fade type=in t=500 wt=1

@move f=モンスターＢ x=0 y=0 w=100 h=100 t=150 wt=1
@move f=モンスターＢ x=0 y=50 w=100 h=100 t=120 wt=1

[ドッペルゲンガー部下/dpg0446]「はっ！　では失礼します」

;;★SE　歩き去る
@se f=歩く_遠
@fade type=out t=2000 wt=0
@move_f f=モンスターＢ face=0 out=c t=1000 wt=1
@bgm

@wait t=2000

@bs wt=0


@ev f=要塞通路

@se f=Earth4
@fade type=in t=500 wt=1

@se f=歩く_遠
@bs f=モンスターＢ face=0 body=-1 opa=0 y=50 t=20 wt=0
@move f=モンスターＢ x=0 y=0 w=100 h=100 opa=255 t=1000 wt=1

　恭しく頭を垂れた後、ドッペルゲンガーの部下が部屋を出る。

[ドッペルゲンガー部下/dpg0447]「ふふふっ、勇者ね。楽しめそうだわ」

;;★SE　ぐにゃぐにゃ
@se f=触手がうごめく音
@move_f f=モンスターＢ face=0 out=c t=250 wt=1

　ニヤッと笑ったドッペルゲンガーの部下の顔が変形し始めると――

　ドッペルゲンガー自身へと変わっていく。

@move_f f=ドッペルゲンガー@元 body=-1 face=0 in=c t=300

[ドッペルゲンガー/dpg0005]「三人には悪いけど、私が味見させてもらうわ」

;;★SE　歩き去る
@se f=歩く_遠

@fade type=out t=1000 wt=0
@move_f f=ドッペルゲンガー@ body=-1 face=0 out=c t=300

@wait t=500

　抜け駆けをする為に部下に変化していたドッペルゲンガーが――

　舌なめずりをしてからユックリと歩き出した。

;;★
;;背景■暗転

;;★
;;効果■時間経過

@bs
@se
@bgm
@bgs
@bgv
@rps

@fade type=in t=20 wt=1

;;============================
