;;============================
;;--------------------------------------------------------------------
;;戦闘イベント：1回目（敗北確定）
;;--------------------------------------------------------------------

;;★BGM　敗北
@fade type=out t=20 wt=1

@move_f f=ナイア_仮@ face=0 body=-1 out=c t=20 wt=0
@bs wt=0

@ev f=要塞内部 t=20 wt=0
@bs f=effect/攻撃開始@5 body=-1 face=0 opa=120 t=20 wt=0
@bs f=ナイア_仮@怒り body=-1 face=0 t=20 wt=0

@move f=要塞内部 x=0 y=0 w=120 h=120 opa=255 t=20 wt=0
@move f=effect/攻撃開始@5 x=0 y=0 w=120 h=120 opa=120 t=20 wt=0

@fade type=in t=20 wt=1

@bgm f=006 pitch=110

;;★白フラ　ｑｋ

@se f=風切り音2
@bs f=effect/衝突@01 body=-1 face=0 opa=0 t=20 wt=0
@move f=effect/衝突@01 x=0 y=0 w=120 h=120 opa=255 t=200 wt=1
@qk
@move f=effect/衝突@01 x=0 y=0 w=120 h=120 opa=0 t=100 wt=1
@move_f f=effect/衝突@01 out=c face=0 t=20 wt=0

[ナイア/nia0004]「ふんっ！」

@bgm

@se f=Blow4
@bs f=effect/引き裂く@02 body=-1 face=0 opa=0 t=20 wt=0
@move f=effect/引き裂く@02 x=0 y=0 w=120 h=120 opa=255 t=100 wt=1
@qk t=500
@move f=effect/引き裂く@02 x=0 y=20 w=120 h=120 opa=0 t=150 wt=1
@move_f f=effect/引き裂く@02 out=c face=0 t=20 wt=0

[リック]「あぐぅっ！？」

　わずらわしそうに腕を振ったナイアの拳によって吹き飛ばされる。

@se f=締め付ける
@qk

[リック]「ぐっ……ぅぅ」

@se f=Fire1
@move f=effect/攻撃開始@5 x=0 y=0 w=300 h=300 opa=0 t=1000 wt=1

@bgm f=003

;;★ZOOM
@move_f f=ナイア_仮@ face=0 out=c t=300 wt=1
@move_f f=ナイア_仮@微笑_z face=0 in=c t=300 wt=1

[ナイア/nia0005]「では、溜まった精力を全て貰ってやるとしよう」

　倒れ込んだ俺の側へとナイアがユックリと近づいてきた。

@fade type=out t=1000 wt=1

@bs
@se
@bgm
@bgs
@bgv
@rps

@fade type=in t=20 wt=1

;;============================
