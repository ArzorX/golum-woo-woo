;;============================
;;------------------------------------------------------
;;戦闘2回目　勝利　　敵を弱く
;;------------------------------------------------------

;;★BGM　勝利
@fade type=out t=20 wt=1

@move_f f=ナイア_仮@ face=0 body=-1 out=c t=20 wt=0
@bs wt=0

@ev f=要塞内部 t=20 wt=0
@bs f=effect/攻撃開始@5 body=-1 face=0 opa=120 t=20 wt=0
@bs f=ナイア_仮@怒り body=-1 face=0 t=20 wt=0

@move f=要塞内部 x=0 y=0 w=120 h=120 opa=255 t=20 wt=0
@move f=effect/攻撃開始@5 x=0 y=0 w=120 h=120 opa=120 t=20 wt=0

@fade type=in t=20 wt=1

@bgm f=006 pitch=110

;;★白フラ　ｑｋ

@se f=風切り音2
@bs f=effect/衝突@01 body=-1 face=0 opa=0 t=20 wt=0
@move f=effect/衝突@01 x=0 y=0 w=120 h=120 opa=255 t=200 wt=1
@qk
@move f=effect/衝突@01 x=0 y=0 w=120 h=120 opa=0 t=100 wt=1
@move_f f=effect/衝突@01 out=c face=0 t=20 wt=0

@se f=風切り音2 pitch=60
@move_f f=effect/AT@02 face=0 in=c t=100 wt=1
@move_f f=effect/AT@02 face=0 out=c t=50 wt=1

@bgm

@se f=Blow4
@flash t=300 wt=0
@move_f f=effect/打撃@03_3 face=0 in=c t=200 wt=1
@qk t=500
@move_f f=effect/打撃@03_3 face=0 out=c t=200 wt=1

@se f=Fire1
@move f=effect/攻撃開始@5 x=0 y=0 w=300 h=300 opa=0 t=1000 wt=1

;;★SE　倒れる　白フラ　ｑｋ
@bs f=ナイア_仮@焦り body=-1 face=0

@move f=ナイア_仮@焦り x=10 y=50 w=100 h=100 opa=255 t=50 wt=1
@move f=ナイア_仮@焦り x=-10 y=50 w=100 h=100 opa=255 t=50 wt=1
@move f=ナイア_仮@焦り x=10 y=50 w=100 h=100 opa=255 t=50 wt=1
@move f=ナイア_仮@焦り x=0 y=50 w=100 h=100 opa=255 t=50 wt=1

[ナイア/nia0063]「ぐっ……」

@move f=ナイア_仮@焦り x=0 y=384 w=100 h=100 opa=0 t=300 wt=1

@se f=床にドサッ
@qk

@move_f f=ナイア_仮@ out=c face=0 t=200

　呻き声を漏らし倒れるナイア。

@bgm f=003

[リック]「はぁっ、はぁっ、はぁっ」

[リック]「倒したの……か？」

　絶命したようにも見えるナイアを見下ろし、息を乱したまま呟く。

[リック]（こんなにあっさりと……？）

　疑問が脳裏を過った時。

;;★BGM停止
@bgm

@bs f=effect/攻撃準備@5 face=0 body=-1 opa=0 t=20 wt=0
@move f=effect/攻撃準備@5 x=0 y=0 w=150 h=150 opa=0 t=20 wt=1

@se f=地鳴りの音
@move f=effect/攻撃準備@5 x=0 y=0 w=110 h=110 opa=255 t=3000 wt=0
@qk t=3000 wt=1

@flash col=255,65,100,255 t=500 wt=0
@qk t=500 wt=1

@se f=Thunder7 pitch=80
@se f=Battle3

@move f=effect/攻撃準備@5 x=0 y=0 w=200 h=200 opa=0 t=100 wt=1

@flash col=255,65,100,255 t=500 wt=0
@qk t=500 wt=1

@se f=「ドクンッ」という心音
@move f=effect/攻撃準備@5 x=0 y=0 w=100 h=100 opa=100 t=20 wt=1
@move f=effect/攻撃準備@5 x=0 y=0 w=110 h=110 opa=0 t=500 wt=0
@qk t=1000

@se f=「ドクンッ」という心音
@move f=effect/攻撃準備@5 x=0 y=0 w=100 h=100 opa=100 t=20 wt=1
@move f=effect/攻撃準備@5 x=0 y=0 w=110 h=110 opa=0 t=500 wt=0
@qk t=1000

　ナイアの体が蠢き始めた。

@bgm f=004

@bs f=effect/エフェクト03@02 face=0 in=c opa=0
@move f=effect/エフェクト03@02 x=0 y=0 w=300 h=300 opa=0 t=20 wt=1

@se f=Growl pitch=90

@move f=effect/エフェクト03@02 x=0 y=0 w=110 h=110 opa=255 t=2000 wt=0
@flash col=0,0,0,255 t=1000 wt=1

@wait t=250

@flash col=0,0,0,255 t=300 wt=1
@flash col=0,0,0,255 t=300 wt=1

@se f=Darkness5

@fade type=out t=200 wt=0
@qk t=200
@fade type=in t=200 wt=0
@qk t=500

@fade type=out t=200 wt=0
@qk t=200
@fade type=in t=200 wt=0
@qk t=200

@fade type=out t=200 wt=0
@qk t=200

@bs wt=0

@fade type=in t=20 wt=1

　得体の知れないナニかが現れる。

@se f=地鳴りの音 pitch=90
@qk
@move_f f=effect/エフェクト01@01 face=0 in=c opa=100 t=200 wt=1
@move_f f=effect/エフェクト01@02 face=0 in=c opa=100 t=200 wt=1
@move_f f=effect/エフェクト01@03 face=0 in=c opa=100 t=200 wt=1
@move_f f=effect/エフェクト01@04 face=0 in=c opa=100 t=200 wt=1
@move_f f=effect/エフェクト01@05 face=0 in=c opa=100 t=200 wt=1
@move_f f=effect/エフェクト01@01 face=0 in=c opa=100 t=200 wt=1
@move_f f=effect/エフェクト01@02 face=0 in=c opa=100 t=200 wt=1
@move_f f=effect/エフェクト01@03 face=0 in=c opa=100 t=200 wt=1
@move_f f=effect/エフェクト01@04 face=0 in=c opa=100 t=200 wt=1
@move_f f=effect/エフェクト01@05 face=0 in=c opa=100 t=200 wt=1

@fade type=out t=200 wt=1

@bs wt=0

@ev f=要塞内部
@bs f=effect/攻撃開始@5 body=-1 face=0 opa=0 t=20 wt=0
@bs f=ナイア_真@蔑み_z body=-1 face=0 opa=0 t=20 wt=0
@bs f=effect/ロボット@00 body=-1 face=0 opa=0 t=20 wt=0
@bs f=effect/エフェクト03@02 body=-1 face=0 opa=0 t=20 wt=0
@bs f=effect/攻撃準備@5 body=-1 face=0 opa=0 t=20 wt=0

@move f=要塞内部 x=0 y=0 w=120 h=120 opa=255 t=20 wt=0
@move f=ナイア_真@蔑み_z x=0 y=-192 w=150 h=150 opa=0 t=20 wt=0
@move f=effect/ロボット@00 x=0 y=0 w=300 h=300 opa=0 t=20 wt=0
@move f=effect/攻撃準備@5 x=0 y=0 w=150 h=150 opa=0 t=20 wt=0
@move f=effect/エフェクト03@02 x=0 y=0 w=200 h=200 opa=0 t=20 wt=1

@fade type=in t=20 wt=1

@se f=Thunder8 pitch=80
@move f=effect/ロボット@00 x=0 y=0 w=120 h=120 opa=255 t=150 wt=1
@move f=effect/ロボット@00 x=0 y=0 w=100 h=100 opa=0 t=100 wt=1
@flash t=1000 wt=0

@se f=地鳴りの音
@move f=effect/攻撃準備@5 x=0 y=0 w=110 h=110 opa=255 t=3000 wt=0
@qk t=3000 wt=1

@flash col=255,65,100,255 t=500 wt=0
@move f=effect/ロボット@00 x=0 y=0 w=120 h=120 opa=0 t=20 wt=0
@move f=effect/攻撃開始@5 x=0 y=0 w=120 h=120 opa=120 t=20 wt=0
@move f=ナイア_真@蔑み_z x=0 y=-192 w=150 h=150 opa=255 t=20 wt=0

@qk t=500 wt=1

@se f=Battle3
@move f=effect/攻撃準備@5 x=0 y=0 w=200 h=200 opa=0 t=100 wt=1
@qk t=1000 wt=1

@bgm

@se f=「ドクンッ」という心音
@flash t=300 wt=0
@qk

[リック]「！！！？」

@se f=Fire6
@move f=effect/エフェクト03@02 x=0 y=0 w=150 h=150 opa=255 t=400 wt=1

@move f=effect/エフェクト03@02 x=0 y=0 w=100 h=100 opa=0 t=200 wt=0
@move f=effect/攻撃準備@5 x=0 y=0 w=120 h=120 opa=120 t=200 wt=1

@move f=effect/エフェクト03@02 x=0 y=0 w=200 h=200 opa=0 t=20 wt=1

@move f=effect/攻撃準備@5 x=0 y=0 w=120 h=120 opa=255 t=500 wt=0
@qk t=500 wt=1

@move f=ナイア_真@蔑み_z x=0 y=0 w=100 h=100 opa=0 t=20 wt=1

@se f=Darkness5
@move f=effect/攻撃準備@5 x=0 y=0 w=200 h=200 opa=0 t=300 wt=0
@move f=effect/攻撃開始@5 x=0 y=0 w=300 h=300 opa=0 t=1000 wt=1
@qk t=1000 wt=1

;;★何か演出出来ればいいなぁ

@se f=Fire1
@flash col=200,65,200,255 t=1000 wt=0
@move f=effect/攻撃開始@5 x=0 y=0 w=120 h=120 opa=120 t=500 wt=0
@qk t=800

@wait t=250

;;★BGM　更に激しい戦い
@bgm f=008 pitch=110

@bs f=ナイア_真@普通_z body=-1 face=0
[ナイア/nia0064]「さぁ、続きをしようか」

[リック]「あ……あぁ……」

　凄惨さを感じさせる美貌と――

　その場に屈してしまいたくなるような強烈な威圧感に言葉が出なくなる。

[リック]（無理だ……勝てない……）

　戦う前から戦意が喪失してしまいそうになる。

@bs f=ナイア_真@微笑 body=-1 face=0
[ナイア/nia0065]「くくっ、どうした？　こないのか？」

[リック]（あ、諦めるな……戦って勝つしかないんだ！　これが最後の戦いだ！）

　怯む心を叱咤しながら、剣を向けナイアに斬りかかる。

;;============================
