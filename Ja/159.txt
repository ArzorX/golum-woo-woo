;;============================
;;★BGM　ピンチ
@bgm f=Scene1

@bg f=異世界街
@fade type=in t=500 wt=1

@bs f=フレッシュゴーレム@怒り body=-1 face=0 x=c
@move f=フレッシュゴーレム@怒り x=0 y=50 w=100 h=100 t=150 wt=1
@move f=フレッシュゴーレム@怒り x=0 y=0 w=100 h=100 t=120 wt=1
@move f=フレッシュゴーレム@怒り x=0 y=50 w=100 h=100 t=150 wt=1
@move f=フレッシュゴーレム@怒り x=0 y=0 w=100 h=100 t=120 wt=1
[フレッシュゴーレム/fls0081]「いたわよー！　こっちにいたわ！」

[リック]「く……っ！」

@bs
@bg f=異世界街
@fade type=in t=500 wt=1

　見世物小屋から少し離れた所まで来ると、背後からフレッシュゴーレムの声が聞こえてきた。

　逃亡に気づいたフレッシュゴーレムの一団が、俺めがけて駆け寄ってくる。

;;★逃げる演出
@ev f=異世界街
@bs f=集中線 face=0 opa=0 body=-1

@se f=走る_遠
@fade type=in t=2000 wt=0
@move f=集中線 x=0 y=0  w=100 h=100 opa=70 t=2000 wt=0

@move f=異世界街 x=0 y=19  w=105 h=105 opa=255 t=200 wt=1
@move f=異世界街 x=0 y=-19 w=110 h=110 opa=255 t=200 wt=1

@move f=異世界街 x=0 y=19  w=115 h=115 opa=255 t=200 wt=1
@move f=異世界街 x=0 y=-19 w=120 h=120 opa=255 t=200 wt=1

@move f=異世界街 x=0 y=19  w=125 h=125 opa=255 t=200 wt=1
@move f=異世界街 x=0 y=-19 w=130 h=130 opa=255 t=200 wt=1

@move f=異世界街 x=0 y=19  w=135 h=135 opa=255 t=200 wt=1
@move f=異世界街 x=0 y=-19 w=140 h=140 opa=255 t=200 wt=1

@move f=異世界街 x=0 y=19  w=145 h=145 opa=255 t=200 wt=1
@move f=異世界街 x=0 y=0   w=150 h=150 opa=255 t=200 wt=1
@move f=集中線 x=0 y=0  w=100 h=100 opa=0 t=100 wt=0

[リック]「はっ！　はっ！　はっ！」

　全速力で駆けながら逃げ切ろうとするが――

　フレッシュゴーレムの追撃は執拗だった。

@ev

;;★白フラ
@flash col=255,255,255,255 t=300
[リック]（くっ……このままじゃ、また捕まる！）

　覚悟を決めて、フレッシュゴーレムの一団を相手に戦うべきか！？

[リック]「……あっ！」

　少し入り組んだ道に小屋が立っている。

　素早く入ればフレッシュゴーレムをやり過ごせるかもしれない。

@bs
@se
@bgm
@bgs
@bgv
@rps

;;============================
