;;============================
;;------------------------------------------------------
;;戦闘2～5回目　勝利
;;------------------------------------------------------

;;★BGM　勝利
@bgm f=Battle5

@bg f=異世界森
@fade type=in t=500 wt=1

;;★剣で切る演出
@se f=斬る音 pitch=90
@move_f f=effect/斬る@00 face=0 in=c t=200 wt=1
@move_f f=effect/斬る@01 face=0 in=c t=200 wt=1
@wait t=300
@bs

@bg f=異世界森
@fade type=in t=500 wt=1

　俺を捕えようとする蔦を斬り上げ――

　返す刃でアルラウネの幹を切断する。

;;★赤フラ　ｑｋ
;;★SE　倒れる
@move_f f=アルラウネ@屈辱 body=-1 face=0 in=c t=300
@flash col=255,0,0,255 t=300
[アルラウネ/alr0066]「ひぁ……ぁああああっ！」

@move_f f=アルラウネ@屈辱 out=c t=200
@se f=床にドサッ
@qk

　悲鳴と共に、バランスが取れずに倒れるアルラウネ。

;;★SE　触手
@se f=触手がうごめく音
@bs f=アルラウネ@焦り face=0 body=-1 x=c

[アルラウネ/alr0067]「うぅ……卑怯よぉ……」

@bs
@bg f=異世界森
@fade type=in t=500 wt=1

;;★SE　歩き去る
@se f=歩く_遠
@wait t=100
　立ち上がれずもがいているアルラウネを一瞥すると、そのままその場を後にした。

;;背景■暗転

;;効果■時間経過
@bgf- bg=A-black t=1500

@bs
@se
@bgm
@bgs
@bgv
@rps

;;============================
