;;============================
;;★BGM　モンスター登場
@bgm f=Scene2

@bg f=湖
@fade type=in t=500 wt=1

@move_f f=ドッペルゲンガー@蔑み body=-1 face=0 in=c t=300
[ドッペルゲンガー/dpg0066]（ちゃんと生き返ったわね……）

@bs
@bg f=湖
@fade type=in t=500 wt=1

　意識を取り戻した俺を見て、ドッペルゲンガーが聞こえないように呟く。

@bs
@se
@bgm
@bgs
@bgv
@rps

;;============================
